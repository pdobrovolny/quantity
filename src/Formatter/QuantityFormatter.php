<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Formatter;

use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;
use PDobrovolny\Quantity\Enums\EDimensions;
use PDobrovolny\Quantity\Enums\EMetricExponent;

final readonly class QuantityFormatter
{
    private UnitFormatter $unitFormatter;
    private \NumberFormatter $numberFormatter;

    public function __construct(?\NumberFormatter $numberFormatter = null)
    {
        $this->numberFormatter = $numberFormatter
            ?? new \NumberFormatter(\Locale::getDefault(), \NumberFormatter::DECIMAL);
        $this->unitFormatter = new UnitFormatter();
    }

    public function format(IQuantity $quantity, ?EMetricExponent $exponent = null): string
    {
        return $quantity instanceof IMetric
            ? $this->formatMetric($quantity, $exponent ?? EMetricExponent::getDefault($quantity))
            : $this->formatDefault($quantity);
    }

    private function formatDefault(IQuantity $quantity): string
    {
        return $this->makeOutput(
            $quantity->value,
            $this->unitFormatter->format($quantity, EMetricExponent::getDefault($quantity))
        );
    }

    private function formatMetric(IQuantity $quantity, EMetricExponent $requestedExponent): string
    {
        $dimensions = EDimensions::fromQuantity($quantity);
        $defaultExponent = EMetricExponent::getDefault($quantity);

        $currentExponent = EMetricExponent::tryFrom($defaultExponent->value - $requestedExponent->value);

        $mainValue = $currentExponent === null
            ? $defaultExponent->getValue($quantity->value, $dimensions)
            : $quantity->value;

        return $this->makeOutput(
            ($currentExponent ?? $requestedExponent)->getValue($mainValue, $dimensions),
            $this->unitFormatter->format($quantity, $requestedExponent)
        );
    }

    private function makeOutput(float $value, string $unit): string
    {
        return \sprintf('%s %s', $this->numberFormatter->format($value), $unit);
    }
}
