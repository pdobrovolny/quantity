<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Formatter;

use Ds\Map;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantityMulti;
use PDobrovolny\Quantity\Enums\EDimensions;
use PDobrovolny\Quantity\Enums\EMetricExponent;

final readonly class UnitFormatter
{
    /** @var Map<string,string> */
    private Map $cache;

    #[Pure] public function __construct()
    {
        $this->cache = new Map([]);
    }

    public function format(IQuantity $quantity, ?EMetricExponent $exponent = null): string
    {
        $exponent ??= EMetricExponent::getDefault($quantity);

        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->cache[$quantity::class . ':' . $exponent->value] ??= $quantity instanceof IQuantityMulti
            ? $this->formatUnitMulti($quantity::getClasses(), $exponent)
            : $this->formatUnit($quantity::class, $exponent);
    }

    /**
     * @param class-string<IQuantity> $class
     *
     * @return list<string>
     */
    private function extractUnit(string $class): array
    {
        return match (true) {
            \is_subclass_of($class, IQuantityMulti::class) => \array_merge(
                ...\array_map($this->extractUnit(...), $class::CLASSES)
            ),
            \is_subclass_of($class, IMetric::class) => [$this->formatUnit($class, EMetricExponent::BASE)],
            default => throw new \InvalidArgumentException('todo')
        };
    }

    /**
     * @param class-string<IQuantity> $class
     */
    private function formatUnit(string $class, EMetricExponent $exponent): string
    {
        $prefix = \is_subclass_of($class, IMetric::class)
            ? $exponent->getSymbol()
            : '';

        return \sprintf('%s%s%s', $prefix, $class::UNIT, EDimensions::fromQuantity($class)->getSymbol());
    }

    /**
     * @param list<class-string<IQuantity>> $quantityClasses
     */
    private function formatUnitMulti(array $quantityClasses, EMetricExponent $exponent): string
    {
        return $this->implode(
            \array_map($this->extractUnit(...), $quantityClasses),
            $exponent
        );
    }

    /**
     * @param list<list<string>> $data
     */
    private function implode(array $data, EMetricExponent $exponent): string
    {
        $values = \array_count_values(\array_merge(...$data));
        \array_walk(
            $values,
            static function (int|string &$count, string $symbol): void {
                $count = $symbol . EDimensions::from($count)->getSymbol();
            }
        );

        return $exponent->getSymbol() . \implode('/', $values);
    }
}
