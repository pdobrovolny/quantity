<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Enums;

use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Enums\IDimensions;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity2D;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity3D;

enum EDimensions: int implements IDimensions
{
    case D1 = 1;
    case D2 = 2;
    case D3 = 3;

    /**
     * @param IQuantity|class-string<IQuantity> $quantity
     */
    public static function fromQuantity(IQuantity|string $quantity): self
    {
        return match (true) {
            \is_subclass_of($quantity, IQuantity3D::class) => self::D3,
            \is_subclass_of($quantity, IQuantity2D::class) => self::D2,
            default => self::D1
        };
    }

    #[Pure] public function getSymbol(): string
    {
        return match ($this) {
            self::D1 => '',
            self::D2 => '²',
            self::D3 => '³'
        };
    }
}
