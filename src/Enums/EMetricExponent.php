<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Enums;

use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Enums\IDimensions;
use PDobrovolny\Quantity\Contracts\Enums\IMetricExponent;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;

enum EMetricExponent: int implements IMetricExponent
{
    case ATTO = -18;
    case BASE = 0;
    case CENTI = -2;
    case DECI = -1;
    case DEKA = 1;
    case EXA = 18;
    case FEMTO = -15;
    case GIGA = 9;
    case HEKTO = 2;
    case IOTTA = 24;
    case KILO = 3;
    case MEGA = 6;
    case MICRO = -6;
    case MILI = -3;
    case NANO = -9;
    case PETA = 15;
    case PIKO = -12;
    case TERA = 12;
    case YOCTO = -24;
    case ZEPTO = -21;
    case ZETTA = 21;

    private const array SYMBOLS = [
        -24 => 'y',
        -21 => 'z',
        -18 => 'a',
        -15 => 'f',
        -12 => 'p',
        -9 => 'n',
        -6 => 'µ',
        -3 => 'm',
        -2 => 'c',
        -1 => 'd',
        0 => '',
        1 => 'da',
        2 => 'h',
        3 => 'k',
        6 => 'M',
        9 => 'G',
        12 => 'T',
        15 => 'P',
        18 => 'E',
        21 => 'Z',
        24 => 'Y',
    ];

    public static function getDefault(IQuantity $quantity): self
    {
        return $quantity instanceof IMass || $quantity instanceof IDensity ? self::KILO : self::BASE;
    }

    public function getSymbol(): string
    {
        return self::SYMBOLS[$this->value];
    }

    #[Pure] public function getValue(float $quantityValue, IDimensions $dimensions): float
    {
        return $quantityValue * (10 ** $dimensions->value) ** $this->value;
    }
}
