<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container;

use JetBrains\PhpStorm\Immutable;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantityMulti;

#[Immutable] abstract readonly class AQuantityMulti implements IQuantityMulti
{
    #[Pure] final public function __construct(public readonly float $value)
    {
    }
}
