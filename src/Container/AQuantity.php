<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container;

use JetBrains\PhpStorm\Immutable;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;

#[Immutable] abstract readonly class AQuantity implements IQuantity
{
    #[Pure] final public function __construct(public float $value)
    {
    }
}
