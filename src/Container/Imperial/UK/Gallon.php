<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Imperial\UK;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGallon;

#[Immutable] final readonly class Gallon extends AQuantity implements IGallon
{
}
