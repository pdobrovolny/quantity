<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Imperial\UK;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IPint;

#[Immutable] final readonly class Pint extends AQuantity implements IPint
{
}
