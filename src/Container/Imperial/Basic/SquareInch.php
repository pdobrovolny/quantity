<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Imperial\Basic;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantityArea;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ISquareInch;

#[Immutable] final readonly class SquareInch extends AQuantityArea implements ISquareInch
{
}
