<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Imperial\Basic;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantityVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ICubicFurlong;

#[Immutable] final readonly class CubicFurlong extends AQuantityVolume implements ICubicFurlong
{
}
