<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Imperial\Basic;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantityVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ICubicLeague;

#[Immutable] final readonly class CubicLeague extends AQuantityVolume implements ICubicLeague
{
}
