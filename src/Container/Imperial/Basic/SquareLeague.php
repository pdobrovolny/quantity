<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Imperial\Basic;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantityArea;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ISquareLeague;

#[Immutable] final readonly class SquareLeague extends AQuantityArea implements ISquareLeague
{
}
