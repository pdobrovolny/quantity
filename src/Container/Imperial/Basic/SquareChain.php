<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Imperial\Basic;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantityArea;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ISquareChain;

#[Immutable] final readonly class SquareChain extends AQuantityArea implements ISquareChain
{
}
