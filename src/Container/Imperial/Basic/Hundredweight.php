<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Imperial\Basic;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IHundredweight;

#[Immutable] final readonly class Hundredweight extends AQuantity implements IHundredweight
{
}
