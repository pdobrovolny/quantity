<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Imperial\US;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IQuart;

#[Immutable] final readonly class Quart extends AQuantity implements IQuart
{
}
