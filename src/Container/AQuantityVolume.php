<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container;

use JetBrains\PhpStorm\Immutable;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity3D;

#[Immutable] abstract readonly class AQuantityVolume implements IQuantity3D
{
    #[Pure] final public function __construct(public readonly float $value)
    {
    }
}
