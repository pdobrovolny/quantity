<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container;

use JetBrains\PhpStorm\Immutable;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity2D;

#[Immutable] abstract readonly class AQuantityArea implements IQuantity2D
{
    #[Pure] final public function __construct(public readonly float $value)
    {
    }
}
