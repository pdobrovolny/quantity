<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Metric\Sub;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ICelsius;

#[Immutable] final readonly class Celsius extends AQuantity implements ICelsius
{
}
