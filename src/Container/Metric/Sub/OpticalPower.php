<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Metric\Sub;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IOpticalPower;

#[Immutable] final readonly class OpticalPower extends AQuantity implements IOpticalPower
{
}
