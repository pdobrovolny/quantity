<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Metric\Sub;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IVoltAmpere;

#[Immutable] final readonly class VoltAmpere extends AQuantity implements IVoltAmpere
{
}
