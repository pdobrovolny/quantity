<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Metric\Derived;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantityMulti;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;

#[Immutable] final readonly class Density extends AQuantityMulti implements IDensity
{
    #[\Override]
    public static function getClasses(): array
    {
        return self::CLASSES;
    }
}
