<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Metric\Derived;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantityMulti;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngularVelocity;

#[Immutable] final readonly class AngularVelocity extends AQuantityMulti implements IAngularVelocity
{
    #[\Override]
    public static function getClasses(): array
    {
        return self::CLASSES;
    }
}
