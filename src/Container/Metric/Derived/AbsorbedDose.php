<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Metric\Derived;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAbsorbedDose;

#[Immutable] final readonly class AbsorbedDose extends AQuantity implements IAbsorbedDose
{
}
