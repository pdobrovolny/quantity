<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Metric\Basic;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IThermodynamicTemperature;

#[Immutable] final readonly class ThermodynamicTemperature extends AQuantity implements IThermodynamicTemperature
{
}
