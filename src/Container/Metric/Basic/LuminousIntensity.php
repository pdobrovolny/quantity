<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Metric\Basic;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILuminousIntensity;

#[Immutable] final readonly class LuminousIntensity extends AQuantity implements ILuminousIntensity
{
}
