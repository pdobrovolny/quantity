<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Container\Metric\Basic;

use JetBrains\PhpStorm\Immutable;
use PDobrovolny\Quantity\Container\AQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IAmountOfSubstance;

#[Immutable] final readonly class AmountOfSubstance extends AQuantity implements IAmountOfSubstance
{
}
