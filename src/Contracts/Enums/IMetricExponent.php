<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Enums;

interface IMetricExponent extends \BackedEnum
{
    public function getSymbol(): string;

    public function getValue(float $quantityValue, IDimensions $dimensions): float;
}
