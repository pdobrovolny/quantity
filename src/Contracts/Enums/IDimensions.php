<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Enums;

interface IDimensions extends \BackedEnum
{

    public function getSymbol(): string;
}
