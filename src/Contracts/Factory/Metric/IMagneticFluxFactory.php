<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFluxDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;

interface IMagneticFluxFactory
{
    public function createByAreaMagneticFluxDensity(
        IArea $area,
        IMagneticFluxDensity $magneticFluxDensity
    ): IMagneticFlux;

    public function createByTimeVoltage(ITime $time, IVoltage $voltage): IMagneticFlux;
}
