<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IThermodynamicTemperature;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ICelsius;

interface ICelsiusFactory
{
    public const float KELVIN_CELSIUS = IThermodynamicTemperatureFactory::KELVIN_CELSIUS;

    public function createByThermodynamicTemperature(IThermodynamicTemperature $thermodynamicTemperature): ICelsius;
}
