<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ILightYear;

interface ILightYearFactory
{
    public const float LENGTH_PER_LIGHT_YEAR = ILengthFactory::LENGTH_PER_LIGHT_YEAR;

    public function createByLength(ILength $length): ILightYear;
}
