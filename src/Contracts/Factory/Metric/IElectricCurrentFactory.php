<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICharge;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IConductance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IResistance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IVoltAmpere;

interface IElectricCurrentFactory
{
    public function createByConductanceVoltage(IConductance $conductance, IVoltage $voltage): IElectricCurrent;

    public function createByPowerVoltage(IPower $power, IVoltage $voltage): IElectricCurrent;

    public function createByResistanceVoltAmpere(IResistance $resistance, IVoltAmpere $voltAmpere): IElectricCurrent;

    public function createByResistanceVoltage(IResistance $resistance, IVoltage $voltage): IElectricCurrent;

    public function createByTimeCharge(ITime $time, ICharge $charge): IElectricCurrent;

    public function createByVoltageVoltAmpere(IVoltage $voltage, IVoltAmpere $voltAmpere): IElectricCurrent;
}
