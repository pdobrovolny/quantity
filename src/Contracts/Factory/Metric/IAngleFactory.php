<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngle;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngularVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDegree;

interface IAngleFactory
{
    public const float ANGLE_MAX = 2 * \M_PI;
    public const float DEGREE_MAX = 360.;
    public const int DEGREE_SUB_UNIT = 60;

    public function createByDegree(IDegree $degree): IAngle;

    public function createByTimeAngularVelocity(ITime $time, IAngularVelocity $angularVelocity): IAngle;
}
