<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcMinute;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcSecond;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDegree;

interface IArcMinuteFactory
{
    public const int DEGREE_SUB_UNIT = IAngleFactory::DEGREE_SUB_UNIT;

    public function arcSecondToArcMinute(IArcSecond $arcSecond): IArcMinute;

    public function degreeToArcMinute(IDegree $degree): IArcMinute;
}
