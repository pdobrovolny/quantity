<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IAmountOfSubstance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngle;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngularVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICatalyticActivity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICharge;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IRadioactivity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolumetricFlowRate;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDay;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IHour;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IMinute;

interface ITimeFactory
{
    public const int SEC_PER_DAY = 24 * self::SEC_PER_HOUR;
    public const int SEC_PER_HOUR = self::SEC_PER_MINUTE ** 2;
    public const int SEC_PER_MINUTE = 60;

    public function createByAccelerationVelocity(IAcceleration $acceleration, IVelocity $velocity): ITime;

    public function createByAmountOfSubstanceCatalyticActivity(
        IAmountOfSubstance $amountOfSubstance,
        ICatalyticActivity $catalyticActivity
    ): ITime;

    public function createByAngleAngularVelocity(IAngle $angle, IAngularVelocity $angularVelocity): ITime;

    public function createByChargeVoltage(ICharge $charge, IVoltage $voltage): ITime;

    public function createByCurrentCharge(IElectricCurrent $electricCurrent, ICharge $charge): ITime;

    public function createByDay(IDay $day): ITime;

    public function createByHour(IHour $hour): ITime;

    public function createByLengthVelocity(ILength $length, IVelocity $velocity): ITime;

    public function createByMagneticFluxVoltage(IMagneticFlux $magneticFlux, IVoltage $voltage): ITime;

    public function createByMinute(IMinute $minute): ITime;

    public function createByPowerWork(IPower $power, IWork $work): ITime;

    public function createByRadioactivity(IRadioactivity $radioactivity): ITime;

    public function createByVolumeVolumetricFlowRate(IVolume $volume, IVolumetricFlowRate $volumetricFlowRate): ITime;
}
