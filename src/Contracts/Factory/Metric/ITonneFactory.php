<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ITonne;

interface ITonneFactory
{
    public function createByMass(IMass $mass): ITonne;
}
