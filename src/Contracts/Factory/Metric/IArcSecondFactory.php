<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcMinute;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcSecond;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDegree;

interface IArcSecondFactory
{
    public const int DEGREE_SUB_UNIT = IAngleFactory::DEGREE_SUB_UNIT;

    public function createByArcMinute(IArcMinute $arcMinute): IArcSecond;

    public function createByDegree(IDegree $degree): IArcSecond;
}
