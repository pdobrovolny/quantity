<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICapacitance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICharge;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;

interface IChargeFactory
{
    public function createByCapacitanceVoltage(ICapacitance $capacitance, IVoltage $voltage): ICharge;

    public function createByCurrentTime(IElectricCurrent $electricCurrent, ITime $time): ICharge;

    public function createByTimeVoltage(ITime $time, IVoltage $voltage): ICharge;

    public function createByVoltageWork(IVoltage $voltage, IWork $work): ICharge;
}
