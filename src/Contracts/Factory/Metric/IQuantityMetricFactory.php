<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Enums\IMetricExponent;
use PDobrovolny\Quantity\Contracts\Factory\IQuantityMainFactory;
use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Enums\EMetricExponent;

/**
 * @template TQuantity of IMetric
 *
 * @template-extends IQuantityMainFactory<TQuantity>
 */
interface IQuantityMetricFactory extends IQuantityMainFactory
{
    public function create(float $value, ?IMetricExponent $exponent = null): IMetric;

    /**
     * @return TQuantity
     */
    public function makeQuantityDivide(float ...$values): IMetric;

    /**
     * @return TQuantity
     */
    public function makeQuantityExp(
        float $mainValue,
        IMetricExponent $requestedExponent = EMetricExponent::BASE,
        IMetricExponent $mainExponent = EMetricExponent::BASE
    ): IMetric;

    /**
     * @return TQuantity
     */
    public function makeQuantityProduct(float ...$values): IMetric;
}
