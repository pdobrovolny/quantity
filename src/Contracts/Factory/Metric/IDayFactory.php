<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDay;

interface IDayFactory
{
    public const int SEC_PER_DAY = ITimeFactory::SEC_PER_DAY;

    public function createByTime(ITime $time): IDay;
}
