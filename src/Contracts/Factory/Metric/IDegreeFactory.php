<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngle;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcMinute;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcSecond;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDegree;

interface IDegreeFactory
{
    public const float ANGLE_MAX = IAngleFactory::ANGLE_MAX;
    public const float DEGREE_MAX = IAngleFactory::DEGREE_MAX;
    public const int DEGREE_SUB_UNIT = IAngleFactory::DEGREE_SUB_UNIT;

    public function createByAngle(IAngle $angle): IDegree;

    public function createByArcMinute(IArcMinute $arcMinute): IDegree;

    public function createByArcSecond(IArcSecond $arcSecond): IDegree;
}
