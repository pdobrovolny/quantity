<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICapacitance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICharge;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IConductance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IResistance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IVoltAmpere;

interface IVoltageFactory
{
    public function createByCapacitanceCharge(ICapacitance $capacitance, ICharge $charge): IVoltage;

    public function createByChargeWork(ICharge $charge, IWork $work): IVoltage;

    public function createByCurrentConductance(IElectricCurrent $electricCurrent, IConductance $conductance): IVoltage;

    public function createByCurrentPower(IElectricCurrent $electricCurrent, IPower $power): IVoltage;

    public function createByCurrentResistance(IElectricCurrent $electricCurrent, IResistance $resistance): IVoltage;

    public function createByCurrentVoltAmpere(IElectricCurrent $electricCurrent, IVoltAmpere $voltAmpere): IVoltage;

    public function createByResistanceVoltAmpere(IResistance $resistance, IVoltAmpere $voltAmpere): IVoltage;

    public function createByTimeCharge(ITime $time, ICharge $charge): IVoltage;

    public function createByTimeMagneticFlux(ITime $time, IMagneticFlux $magneticFlux): IVoltage;
}
