<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngularVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IFrequency;

interface IFrequencyFactory
{
    public const float ANGLE_MAX = IAngleFactory::ANGLE_MAX;

    public function createByAngularVelocity(IAngularVelocity $angularVelocity): IFrequency;
}
