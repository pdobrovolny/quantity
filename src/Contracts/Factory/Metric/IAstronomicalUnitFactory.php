<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IAstronomicalUnit;

interface IAstronomicalUnitFactory
{
    public const float LENGTH_PER_ASTRO = ILengthFactory::LENGTH_PER_ASTRO;

    public function createByLength(ILength $length): IAstronomicalUnit;
}
