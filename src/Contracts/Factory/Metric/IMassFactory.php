<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IGrain;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IHundredweight;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IPound;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IStone;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ITon;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAbsorbedDose;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IEquivalentDose;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IAtomicMassUnit;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ITonne;

interface IMassFactory
{
    public const float ATOMIC_MASS = 1.66057 * 10 ** -27;
    public const float METRIC_GRAIN = 0.000_064_798_91;
    public const float METRIC_HW = 50.802_345_44;
    public const float METRIC_OUNCE = 0.028_349_523_125;
    public const float METRIC_POUND = 0.453_592_37;
    public const float METRIC_STONE = 6.350_293_18;
    public const float METRIC_TON = 1_016.046_908_8;

    public function createByAbsorbedDoseWork(IAbsorbedDose $absorbedDose, IWork $work): IMass;

    public function createByAccelerationForce(IAcceleration $acceleration, IForce $force): IMass;

    public function createByAtomicMassUnit(IAtomicMassUnit $atomicMassUnit): IMass;

    public function createByEquivalentDoseWork(IEquivalentDose $equivalentDose, IWork $work): IMass;

    public function createByGrain(IGrain $grain): IMass;

    public function createByHundredweight(IHundredweight $hundredweight): IMass;

    public function createByLengthAccelerationWork(ILength $length, IAcceleration $acceleration, IWork $work): IMass;

    public function createByPound(IPound $pound): IMass;

    public function createByStone(IStone $stone): IMass;

    public function createByTon(ITon $ton): IMass;

    public function createByTonne(ITonne $tonne): IMass;

    public function createByVelocityWork(IVelocity $velocity, IWork $work): IMass;

    public function createByVolumeDensity(IVolume $volume, IDensity $density): IMass;
}
