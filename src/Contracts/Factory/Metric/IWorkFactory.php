<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAbsorbedDose;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICharge;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IEquivalentDose;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;

interface IWorkFactory
{
    public function createByAreaForce(IArea $area, IForce $force): IWork;

    public function createByChargeVoltage(ICharge $charge, IVoltage $voltage): IWork;

    public function createByLengthAreaPressure(ILength $length, IArea $area, IPressure $pressure): IWork;

    public function createByLengthForce(ILength $length, IForce $force): IWork;

    public function createByLengthMassAcceleration(ILength $length, IMass $mass, IAcceleration $acceleration): IWork;

    public function createByMassAbsorbedDose(IMass $mass, IAbsorbedDose $absorbedDose): IWork;

    public function createByMassEquivalentDose(IMass $mass, IEquivalentDose $equivalentDose): IWork;

    public function createByMassVelocity(IMass $mass, IVelocity $velocity): IWork;

    public function createByPressureVolume(IPressure $pressure, IVolume $volume): IWork;

    public function createByTimePower(ITime $time, IPower $power): IWork;
}
