<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IAmountOfSubstance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICatalyticActivity;

interface IAmountOfSubstanceFactory
{
    public function createByTimeCatalyticActivity(
        ITime $time,
        ICatalyticActivity $catalyticActivity
    ): IAmountOfSubstance;
}
