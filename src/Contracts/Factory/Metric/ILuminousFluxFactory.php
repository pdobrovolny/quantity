<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILuminousIntensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IIlluminance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ILuminousFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ISolidAngle;

interface ILuminousFluxFactory
{
    public function createByAreaIlluminance(IArea $area, IIlluminance $illuminance): ILuminousFlux;

    public function createByLuminousIntensitySolidAngle(
        ILuminousIntensity $luminousIntensity,
        ISolidAngle $solidAngle
    ): ILuminousFlux;
}
