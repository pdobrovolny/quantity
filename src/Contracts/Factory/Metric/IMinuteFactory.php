<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IMinute;

interface IMinuteFactory
{
    public const int SEC_PER_MINUTE = ITimeFactory::SEC_PER_MINUTE;

    public function createByTime(ITime $time): IMinute;
}
