<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngle;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngularVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IFrequency;

interface IAngularVelocityFactory
{
    public const float ANGLE_MAX = IAngleFactory::ANGLE_MAX;

    public function createByFrequency(IFrequency $frequency): IAngularVelocity;

    public function createByTimeAngle(ITime $time, IAngle $angle): IAngularVelocity;
}
