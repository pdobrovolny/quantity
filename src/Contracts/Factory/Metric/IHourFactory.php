<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IHour;

interface IHourFactory
{
    public const int SEC_PER_HOUR = ITimeFactory::SEC_PER_HOUR;

    public function createByTime(ITime $time): IHour;
}
