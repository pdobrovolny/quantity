<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IConductance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IResistance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IVoltAmpere;

interface IResistanceFactory
{
    public function createByConductance(IConductance $conductance): IResistance;

    public function createByCurrentVoltAmpere(IElectricCurrent $electricCurrent, IVoltAmpere $voltAmpere): IResistance;

    public function createByCurrentVoltage(IElectricCurrent $electricCurrent, IVoltage $voltage): IResistance;

    public function createByVoltageVoltAmpere(IVoltage $voltage, IVoltAmpere $voltAmpere): IResistance;
}
