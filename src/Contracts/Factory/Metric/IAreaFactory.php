<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IIlluminance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ILuminousFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFluxDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IHectare;

interface IAreaFactory
{
    public function createByForcePressure(IForce $force, IPressure $pressure): IArea;

    public function createByForceWork(IForce $force, IWork $work): IArea;

    public function createByHectare(IHectare $hectare): IArea;

    public function createByIlluminanceLuminousFlux(IIlluminance $illuminance, ILuminousFlux $luminousFlux): IArea;

    public function createByLengthAccelerationForceDensity(
        ILength $length,
        IAcceleration $acceleration,
        IForce $force,
        IDensity $density
    ): IArea;

    public function createByLengthPressureWork(ILength $length, IPressure $pressure, IWork $work): IArea;

    public function createByMagneticFluxMagneticFluxDensity(
        IMagneticFlux $magneticFlux,
        IMagneticFluxDensity $magneticFluxDensity
    ): IArea;
}
