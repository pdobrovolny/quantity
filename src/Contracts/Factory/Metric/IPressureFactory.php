<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;

interface IPressureFactory
{
    public function createByAreaForce(IArea $area, IForce $force): IPressure;

    public function createByLengthAccelerationDensity(
        ILength $length,
        IAcceleration $acceleration,
        IDensity $density
    ): IPressure;

    public function createByLengthAreaWork(ILength $length, IArea $area, IWork $work): IPressure;

    public function createByVolumeWork(IVolume $volume, IWork $work): IPressure;
}
