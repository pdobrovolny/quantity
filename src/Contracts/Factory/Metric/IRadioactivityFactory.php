<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IRadioactivity;

interface IRadioactivityFactory
{
    public function timeToRadioactivity(ITime $time): IRadioactivity;
}
