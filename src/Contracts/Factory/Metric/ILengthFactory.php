<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IChain;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFoot;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFurlong;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IInch;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ILeague;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IYard;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IAstronomicalUnit;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ILightYear;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IOpticalPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IParsec;

interface ILengthFactory
{
    public const float LENGTH_PER_ASTRO = 1.495_98 * 10 ** 11;
    public const float LENGTH_PER_LIGHT_YEAR = 9.4605 * 10 ** 15;
    public const float LENGTH_PER_PARSEC = 3.08567758 * 10 ** 16;
    public const float METRIC_CHAIN = 20.116_8;
    public const float METRIC_FOOT = 0.304_8;
    public const float METRIC_FUR = 201.168;
    public const float METRIC_INCH = 0.025_4;
    public const float METRIC_LEA = 4_828.032;
    public const float METRIC_MILE = 1_609.344;
    public const float METRIC_YARD = 0.914_4;

    public function createByAccelerationAreaForceDensity(
        IAcceleration $acceleration,
        IArea $area,
        IForce $force,
        IDensity $density
    ): ILength;

    public function createByAccelerationPressureDensity(
        IAcceleration $acceleration,
        IPressure $pressure,
        IDensity $density
    ): ILength;

    public function createByAreaPressureWork(IArea $area, IPressure $pressure, IWork $work): ILength;

    public function createByAstronomicalUnit(IAstronomicalUnit $astronomicalUnit): ILength;

    public function createByChain(IChain $chain): ILength;

    public function createByFoot(IFoot $foot): ILength;

    public function createByForceWork(IForce $force, IWork $work): ILength;

    public function createByFurlong(IFurlong $furlong): ILength;

    public function createByInch(IInch $inch): ILength;

    public function createByLeague(ILeague $league): ILength;

    public function createByLightYear(ILightYear $lightYear): ILength;

    public function createByMassAccelerationWork(IMass $mass, IAcceleration $acceleration, IWork $work): ILength;

    public function createByMile(IMile $mile): ILength;

    public function createByOpticalPower(IOpticalPower $opticalPower): ILength;

    public function createByParsec(IParsec $parsec): ILength;

    public function createByTimeVelocity(ITime $time, IVelocity $velocity): ILength;

    public function createByYard(IYard $yard): ILength;
}
