<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILuminousIntensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ILuminousFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ISolidAngle;

interface ISolidAngleFactory
{
    public function createByLuminousIntensityLuminousFlux(
        ILuminousIntensity $luminousIntensity,
        ILuminousFlux $luminousFlux
    ): ISolidAngle;
}
