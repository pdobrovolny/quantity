<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IParsec;

interface IParsecFactory
{
    public const float LENGTH_PER_PARSEC = ILengthFactory::LENGTH_PER_PARSEC;

    public function createByLength(ILength $length): IParsec;
}
