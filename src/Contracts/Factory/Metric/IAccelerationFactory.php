<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;

interface IAccelerationFactory
{
    public function createByLengthAreaForceDensity(
        ILength $length,
        IArea $area,
        IForce $force,
        IDensity $density
    ): IAcceleration;

    public function createByLengthMassWork(ILength $length, IMass $mass, IWork $work): IAcceleration;

    public function createByLengthPressureDensity(
        ILength $length,
        IPressure $pressure,
        IDensity $density
    ): IAcceleration;

    public function createByMassForce(IMass $mass, IForce $force): IAcceleration;

    public function createByTimeVelocity(ITime $time, IVelocity $velocity): IAcceleration;
}
