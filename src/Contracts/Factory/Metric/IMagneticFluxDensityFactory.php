<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFluxDensity;

interface IMagneticFluxDensityFactory
{
    public function createByAreaMagneticFlux(IArea $area, IMagneticFlux $magneticFlux): IMagneticFluxDensity;
}
