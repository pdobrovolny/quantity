<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;

interface IDensityFactory
{
    public function createByLengthAccelerationAreaForce(
        ILength $length,
        IAcceleration $acceleration,
        IArea $area,
        IForce $force
    ): IDensity;

    public function createByLengthAccelerationPressure(
        ILength $length,
        IAcceleration $acceleration,
        IPressure $pressure
    ): IDensity;

    public function createByMassVolume(IMass $mass, IVolume $volume): IDensity;
}
