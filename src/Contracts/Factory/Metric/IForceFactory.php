<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;

interface IForceFactory
{
    public function createByAreaPressure(IArea $area, IPressure $pressure): IForce;

    public function createByAreaWork(IArea $area, IWork $work): IForce;

    public function createByLengthAccelerationAreaDensity(
        ILength $length,
        IAcceleration $acceleration,
        IArea $area,
        IDensity $density
    ): IForce;

    public function createByLengthWork(ILength $length, IWork $work): IForce;

    public function createByMassAcceleration(IMass $mass, IAcceleration $acceleration): IForce;

    public function createByPowerVelocity(IPower $power, IVelocity $velocity): IForce;
}
