<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IResistance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IVoltAmpere;

interface IVoltAmpereFactory
{
    public function createByCurrentResistance(IElectricCurrent $electricCurrent, IResistance $resistance): IVoltAmpere;

    public function createByCurrentVoltage(IElectricCurrent $electricCurrent, IVoltage $voltage): IVoltAmpere;

    public function createByResistanceVoltage(IResistance $resistance, IVoltage $voltage): IVoltAmpere;
}
