<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFahrenheit;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IThermodynamicTemperature;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ICelsius;

interface IThermodynamicTemperatureFactory
{
    public const float KELVIN_CELSIUS = 273.15;

    public function celsiusToThermodynamicTemperature(ICelsius $celsius): IThermodynamicTemperature;

    public function fahrenheitToThermodynamicTemperature(IFahrenheit $fahrenheit): IThermodynamicTemperature;
}
