<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IFluidOunce as IFluidOunceUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGallon as IGallonUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGill as IGillUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IPint as IPintUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IQuart as IQuartUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IFluidOunce as IFluidOunceUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGallon as IGallonUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGill as IGillUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IPint as IPintUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IQuart as IQuartUS;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolumetricFlowRate;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ILiter;

interface IVolumeFactory
{
    public const float METRIC_FL_OZ_UK = 0.000_028_413_062_5;
    public const float METRIC_FL_OZ_US = 2.957_352_956_25 * 10 ** -05;
    public const float METRIC_GALLON_UK = 0.004_546_09;
    public const float METRIC_GALLON_US = 0.003_785_411_784;
    public const float METRIC_GILL_UK = 0.000_142_065_312_5;
    public const float METRIC_GILL_US = 0.000_118_294_118_25;
    public const float METRIC_PINT_UK = 0.000_568_261_25;
    public const float METRIC_PINT_US = 0.000_473_176_473;
    public const float METRIC_QUART_UK = 0.001_136_522_5;
    public const float METRIC_QUART_US = 0.000_946_352_946;

    public function createByFluidOunceUK(IFluidOunceUK $fluidOunce): IVolume;

    public function createByFluidOunceUS(IFluidOunceUS $fluidOunce): IVolume;

    public function createByGallonUK(IGallonUK $gallon): IVolume;

    public function createByGallonUS(IGallonUS $gallon): IVolume;

    public function createByGillUK(IGillUK $gill): IVolume;

    public function createByGillUS(IGillUS $gill): IVolume;

    public function createByLiter(ILiter $liter): IVolume;

    public function createByMassDensity(IMass $mass, IDensity $density): IVolume;

    public function createByPintUK(IPintUK $pint): IVolume;

    public function createByPintUS(IPintUS $pint): IVolume;

    public function createByPressureWork(IPressure $pressure, IWork $work): IVolume;

    public function createByQuartUK(IQuartUK $quart): IVolume;

    public function createByQuartUS(IQuartUS $quart): IVolume;

    public function createByTimeVolumetricFlowRate(ITime $time, IVolumetricFlowRate $volumetricFlowRate): IVolume;
}
