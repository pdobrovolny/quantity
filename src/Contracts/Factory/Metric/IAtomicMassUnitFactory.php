<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IAtomicMassUnit;

interface IAtomicMassUnitFactory
{
    public const float MASS_PER_ATOMIC_MASS = IMassFactory::ATOMIC_MASS;

    public function massToAtomicMassUnit(IMass $mass): IAtomicMassUnit;
}
