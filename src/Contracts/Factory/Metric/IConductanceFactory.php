<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Metric;

use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IConductance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IResistance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;

interface IConductanceFactory
{
    public function createByCurrentVoltage(IElectricCurrent $electricCurrent, IVoltage $voltage): IConductance;

    public function createByResistance(IResistance $resistance): IConductance;
}
