<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory;

use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;
use PDobrovolny\Quantity\Enums\EDimensions;

/**
 * @template TQuantity of IQuantity
 */
interface IQuantityMainFactory
{

    public string $class {
        get;
    }
    public string $interface {
        get;
    }
    public EDimensions $dimensions {
        get;
    }

    /**
     * @return TQuantity
     */
    public function create(float $value): IQuantity;
}
