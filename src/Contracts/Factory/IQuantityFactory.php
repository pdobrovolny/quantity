<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory;

use PDobrovolny\Quantity\Contracts\Enums\IMetricExponent;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;

interface IQuantityFactory
{
    /**
     * @template T of IQuantity
     *
     * @param class-string<T> $interface
     * @param non-empty-array<string,IQuantity|IMetricExponent|float> $parameters
     *
     * @return T
     */
    public function create(string $interface, array $parameters): IQuantity;

    /**
     * @template T of IQuantity
     *
     * @param class-string<T> $interface
     *
     * @return T
     */
    public function createWithValue(string $interface, float $value, ?IMetricExponent $exponent = null): IQuantity;
}
