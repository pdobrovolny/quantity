<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial\US;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IVolumeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IFluidOunce;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGill;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IQuart;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;

interface IPintFactory
{
    public const float METRIC_PINT_US = IVolumeFactory::METRIC_PINT_US;
    public const int OUNCE_PER_PINT = 20;
    public const int PINT_PER_GILL = 4;
    public const int PINT_PER_QUART = 2;

    public function fluidOunceUS(IFluidOunce $fluidOunce): IPint;

    public function gillUS(IGill $gill): IPint;

    public function quartUS(IQuart $quart): IPint;

    public function volume(IVolume $volume): IPint;
}
