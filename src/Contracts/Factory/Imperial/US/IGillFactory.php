<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial\US;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IVolumeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGill;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;

interface IGillFactory
{
    public const float METRIC_GILL_US = IVolumeFactory::METRIC_GILL_US;
    public const int PINT_PER_GILL = 4;

    public function createByPintUS(IPint $pint): IGill;

    public function createByVolume(IVolume $volume): IGill;
}
