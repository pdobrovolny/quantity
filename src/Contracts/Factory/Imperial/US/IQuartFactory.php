<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial\US;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IVolumeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGallon;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IQuart;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;

interface IQuartFactory
{
    public const float METRIC_QUART_US = IVolumeFactory::METRIC_QUART_US;
    public const int PINT_PER_QUART = 2;
    public const int QUART_PER_GALLON = 4;

    public function createByGallonUS(IGallon $gallon): IQuart;

    public function createByPintUS(IPint $pint): IQuart;

    public function createByVolume(IVolume $volume): IQuart;
}
