<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial\US;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IVolumeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGallon;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IQuart;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;

interface IGallonFactory
{
    public const float METRIC_GALLON_US = IVolumeFactory::METRIC_GALLON_US;
    public const int QUART_PER_GALLON = 4;

    public function createByQuartUS(IQuart $quart): IGallon;

    public function createByVolume(IVolume $volume): IGallon;
}
