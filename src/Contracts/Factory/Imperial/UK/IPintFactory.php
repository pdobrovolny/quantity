<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial\UK;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IVolumeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IFluidOunce;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGill;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IQuart;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;

interface IPintFactory
{
    public const float METRIC_PINT_UK = IVolumeFactory::METRIC_PINT_UK;
    public const int OUNCE_PER_PINT = 20;
    public const int PINT_PER_GILL = 4;
    public const int PINT_PER_QUART = 2;

    public function createByFluidOunceUK(IFluidOunce $fluidOunce): IPint;

    public function createByGillUK(IGill $gill): IPint;

    public function createByQuartUK(IQuart $quart): IPint;

    public function createByVolume(IVolume $volume): IPint;
}
