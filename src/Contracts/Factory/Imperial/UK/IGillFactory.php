<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial\UK;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IVolumeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGill;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;

interface IGillFactory
{
    public const float METRIC_GILL_UK = IVolumeFactory::METRIC_GILL_UK;
    public const int PINT_PER_GILL = 4;

    public function createByPintUK(IPint $pint): IGill;

    public function createByVolume(IVolume $volume): IGill;
}
