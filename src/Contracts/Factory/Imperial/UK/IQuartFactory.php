<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial\UK;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IVolumeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGallon;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IQuart;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;

interface IQuartFactory
{
    public const float METRIC_QUART_UK = IVolumeFactory::METRIC_QUART_UK;
    public const int PINT_PER_QUART = 2;
    public const int QUART_PER_GALLON = 4;

    public function createByGallonUK(IGallon $gallon): IQuart;

    public function createByPintUK(IPint $pint): IQuart;

    public function createByVolume(IVolume $volume): IQuart;
}
