<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial\UK;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IVolumeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IFluidOunce;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;

interface IFluidOunceFactory
{
    public const float METRIC_FL_OZ_UK = IVolumeFactory::METRIC_FL_OZ_UK;
    public const int OUNCE_PER_PINT = 20;

    public function createByPint(IPint $pint): IFluidOunce;

    public function createByVolume(IVolume $volume): IFluidOunce;
}
