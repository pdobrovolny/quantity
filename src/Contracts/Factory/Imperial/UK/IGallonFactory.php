<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial\UK;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IVolumeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGallon;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IQuart;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;

interface IGallonFactory
{
    public const float METRIC_GALLON_UK = IVolumeFactory::METRIC_GALLON_UK;
    public const int QUART_PER_GALLON = 4;

    public function createByQuartUK(IQuart $quart): IGallon;

    public function createByVolume(IVolume $volume): IGallon;
}
