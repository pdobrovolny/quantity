<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\ILengthFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IChain;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFurlong;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;

interface IFurlongFactory
{
    public const int CHAIN_PER_FURLONG = 10;
    public const int FURLONG_PER_MILE = 8;
    public const float METRIC_FUR = ILengthFactory::METRIC_FUR;

    public function createByChain(IChain $chain): IFurlong;

    public function createByLength(ILength $length): IFurlong;

    public function createByMile(IMile $mile): IFurlong;
}
