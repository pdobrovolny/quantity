<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IMassFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IHundredweight;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IStone;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ITon;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;

interface IHundredweightFactory
{
    public const int HUNDREDWEIGHT_PER_TON = 20;
    public const float METRIC_HW = IMassFactory::METRIC_HW;
    public const int STONES_PER_HUNDREDWEIGHT = 8;

    public function createByMass(IMass $mass): IHundredweight;

    public function createByStone(IStone $stone): IHundredweight;

    public function createByTon(ITon $ton): IHundredweight;
}
