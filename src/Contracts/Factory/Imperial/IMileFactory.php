<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\ILengthFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFoot;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFurlong;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ILeague;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IYard;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;

interface IMileFactory
{
    public const int FOOT_PER_MILE = 5_280;
    public const int FURLONG_PER_MILE = 8;
    public const int LEAGUE_PER_MILE = 3;
    public const float METRIC_MILE = ILengthFactory::METRIC_MILE;
    public const int YARD_PER_MILE = 1_760;

    public function createByFoot(IFoot $foot): IMile;

    public function createByFurlong(IFurlong $furlong): IMile;

    public function createByLeague(ILeague $league): IMile;

    public function createByLength(ILength $length): IMile;

    public function createByYard(IYard $yard): IMile;
}
