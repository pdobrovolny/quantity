<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\IQuantityMainFactory;
use PDobrovolny\Quantity\Contracts\Quantity\IImperial;

/**
 * @template TQuantity of IImperial
 *
 * @template-extends IQuantityMainFactory<TQuantity>
 */
interface IQuantityImperialFactory extends IQuantityMainFactory
{
}
