<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IMassFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IGrain;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;

interface IGrainFactory
{
    public const float GRAIN_PER_OUNCE = 437.5;
    public const float METRIC_GRAIN = IMassFactory::METRIC_GRAIN;

    public function createByMass(IMass $mass): IGrain;
}
