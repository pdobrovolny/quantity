<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IThermodynamicTemperatureFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFahrenheit;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IThermodynamicTemperature;

interface IFahrenheitFactory
{
    public const float KELVIN_CELSIUS = IThermodynamicTemperatureFactory::KELVIN_CELSIUS;

    public function createByThermodynamicTemperature(IThermodynamicTemperature $thermodynamicTemperature): IFahrenheit;
}
