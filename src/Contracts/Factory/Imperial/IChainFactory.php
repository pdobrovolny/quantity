<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\ILengthFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IChain;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFurlong;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IYard;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;

interface IChainFactory
{
    public const int CHAIN_PER_FURLONG = 10;
    public const float METRIC_CHAIN = ILengthFactory::METRIC_CHAIN;
    public const int YARD_PER_CHAIN = 22;

    public function createByFurlong(IFurlong $furlong): IChain;

    public function createByLength(ILength $length): IChain;

    public function createByYard(IYard $yard): IChain;
}
