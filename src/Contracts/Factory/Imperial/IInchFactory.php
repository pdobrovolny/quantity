<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\ILengthFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFoot;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IInch;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;

interface IInchFactory
{
    public const int INCH_PER_FOOT = 12;
    public const float METRIC_INCH = ILengthFactory::METRIC_INCH;

    public function createByFoot(IFoot $foot): IInch;

    public function createByLength(ILength $length): IInch;
}
