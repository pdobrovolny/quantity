<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\ILengthFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IChain;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFoot;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IYard;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;

interface IYardFactory
{
    public const int FOOT_PER_YARD = 3;
    public const float METRIC_YARD = ILengthFactory::METRIC_YARD;
    public const int YARD_PER_CHAIN = 22;
    public const int YARD_PER_MILE = 1_760;

    public function createByChain(IChain $chain): IYard;

    public function createByFoot(IFoot $foot): IYard;

    public function createByLength(ILength $length): IYard;

    public function createByMile(IMile $mile): IYard;
}
