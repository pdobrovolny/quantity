<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IMassFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IHundredweight;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IPound;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IStone;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;

interface IStoneFactory
{
    public const float METRIC_STONE = IMassFactory::METRIC_STONE;
    public const int POUNDS_PER_STONE = 14;
    public const int STONES_PER_HUNDREDWEIGHT = 8;

    public function createByHundredweight(IHundredweight $hundredweight): IStone;

    public function createByMass(IMass $mass): IStone;

    public function createByPound(IPound $pound): IStone;
}
