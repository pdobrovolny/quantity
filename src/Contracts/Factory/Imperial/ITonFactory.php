<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IMassFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IHundredweight;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ITon;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;

interface ITonFactory
{
    public const int HUNDREDWEIGHT_PER_TON = 20;
    public const float METRIC_TON = IMassFactory::METRIC_TON;

    public function createByHundredweight(IHundredweight $hundredweight): ITon;

    public function createByMass(IMass $mass): ITon;
}
