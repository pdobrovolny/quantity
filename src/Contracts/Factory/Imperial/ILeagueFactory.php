<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\ILengthFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ILeague;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;

interface ILeagueFactory
{
    public const int LEAGUE_PER_MILE = 3;
    public const float METRIC_LEA = ILengthFactory::METRIC_LEA;

    public function createByLength(ILength $length): ILeague;

    public function createByMile(IMile $mile): ILeague;
}
