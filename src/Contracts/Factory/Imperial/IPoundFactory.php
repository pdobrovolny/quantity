<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IMassFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IPound;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IStone;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;

interface IPoundFactory
{
    public const float METRIC_POUND = IMassFactory::METRIC_POUND;
    public const int OUNCE_PER_POUND = 16;
    public const int POUND_PER_STONE = 14;

    public function createByMass(IMass $mass): IPound;

    public function createByStone(IStone $stone): IPound;
}
