<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Factory\Imperial;

use PDobrovolny\Quantity\Contracts\Factory\Metric\ILengthFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFoot;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IInch;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IYard;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;

interface IFootFactory
{
    public const int FOOT_PER_MILE = 5_280;
    public const int FOOT_PER_YARD = 3;
    public const int INCH_PER_FOOT = 12;
    public const float METRIC_FOOT = ILengthFactory::METRIC_FOOT;

    public function inch(IInch $inch): IFoot;

    public function length(ILength $length): IFoot;

    public function mile(IMile $mile): IFoot;

    public function yard(IYard $yard): IFoot;
}
