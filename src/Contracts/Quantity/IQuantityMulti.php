<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity;

interface IQuantityMulti extends IQuantity
{
    /**
     * @return list<class-string<IQuantity>>
     */
    public static function getClasses(): array;
}
