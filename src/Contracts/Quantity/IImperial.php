<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity;

interface IImperial extends IQuantity
{
}
