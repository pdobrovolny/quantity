<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic;

use PDobrovolny\Quantity\Contracts\Quantity\IImperial;

interface ISquareYard extends IImperial
{
    public const string UNIT = IYard::UNIT;
}
