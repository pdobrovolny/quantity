<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Imperial\US;

use PDobrovolny\Quantity\Contracts\Quantity\Imperial\IImperialUS;

interface IGallon extends IImperialUS
{
    public const string UNIT = 'US liq·gal';
}
