<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK;

use PDobrovolny\Quantity\Contracts\Quantity\Imperial\IImperialUK;

interface IFluidOunce extends IImperialUK
{
    public const string UNIT = 'UK fl·oz';
}
