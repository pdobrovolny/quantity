<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Imperial;

use PDobrovolny\Quantity\Contracts\Quantity\IImperial;

interface IImperialUS extends IImperial
{
}
