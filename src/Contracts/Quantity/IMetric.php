<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity;

interface IMetric extends IQuantity
{
}
