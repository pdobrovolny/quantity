<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity;

use JetBrains\PhpStorm\Immutable;

#[Immutable] interface IQuantity
{
    public float $value {
        get;
    }

    public function __construct(float $value);
}
