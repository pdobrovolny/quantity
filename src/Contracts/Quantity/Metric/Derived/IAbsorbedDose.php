<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived;

use PDobrovolny\Quantity\Contracts\Quantity\IMetric;

interface IAbsorbedDose extends IMetric
{
    public const string UNIT = 'Gy';
}
