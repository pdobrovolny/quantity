<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived;

use PDobrovolny\Quantity\Contracts\Quantity\IMetric;

interface IFrequency extends IMetric
{
    public const string UNIT = 'Hz';
}
