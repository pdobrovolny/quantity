<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived;

use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity2D;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;

interface IArea extends IMetric, IQuantity2D
{
    public const string UNIT = ILength::UNIT;
}
