<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived;

use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity3D;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;

interface IVolume extends IMetric, IQuantity3D
{
    public const string UNIT = ILength::UNIT;
}
