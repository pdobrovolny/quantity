<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived;

use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantityMulti;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;

interface IVelocity extends IMetric, IQuantityMulti
{
    public const array CLASSES = [ILength::class, ITime::class];
}
