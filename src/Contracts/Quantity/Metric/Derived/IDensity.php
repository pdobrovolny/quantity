<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived;

use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantityMulti;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;

interface IDensity extends IQuantityMulti, IMetric
{
    public const array CLASSES = [IMass::class, IVolume::class];
}
