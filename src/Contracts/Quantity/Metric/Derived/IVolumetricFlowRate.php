<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived;

use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity3D;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;

interface IVolumetricFlowRate extends IMetric, IQuantity3D
{
    public const array CLASSES = [IVolume::class, ITime::class];
}
