<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived;

use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantityMulti;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;

interface IAcceleration extends IMetric, IQuantityMulti
{
    public const float GRAVITATIONAL_CONSTANT = 9.80665;
    public const array CLASSES = [IVelocity::class, ITime::class];
}
