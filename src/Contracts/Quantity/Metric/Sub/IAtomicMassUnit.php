<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub;

use PDobrovolny\Quantity\Contracts\Quantity\IMetric;

interface IAtomicMassUnit extends IMetric
{
    public const string UNIT = 'u';
}
