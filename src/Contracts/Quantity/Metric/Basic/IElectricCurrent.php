<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic;

use PDobrovolny\Quantity\Contracts\Quantity\IMetric;

interface IElectricCurrent extends IMetric
{
    public const string UNIT = 'A';
}
