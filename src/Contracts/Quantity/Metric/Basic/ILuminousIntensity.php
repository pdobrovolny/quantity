<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic;

use PDobrovolny\Quantity\Contracts\Quantity\IMetric;

interface ILuminousIntensity extends IMetric
{
    public const string UNIT = 'cd';
}
