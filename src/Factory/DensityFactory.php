<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory;

use Ds\Map;
use PDobrovolny\Quantity\Contracts\Factory\IQuantityFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;

final readonly class DensityFactory
{
    /**  @var Map<string,IDensity> */
    private Map $densities;

    public function __construct(private IQuantityFactory $quantityFactory)
    {
        $this->densities = new Map([]);
    }

    public function acetone(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(789.9);
    }

    public function aluminium(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(2700.);
    }

    public function benzine(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity((700. + 750.) / 2);
    }

    public function carbon(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(2250.);
    }

    /**
     * Obsahuje 96–98 % metanu.
     */
    public function cng(): IDensity
    {
        // https://www.tzb-info.cz/tabulky-a-vypocty/90-hustota-zemnich-plynu-v-zavislosti-na-teplote
        // https://cs.wikipedia.org/wiki/Stla%C4%8Den%C3%BD_zemn%C3%AD_plyn
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(678.);
    }

    public function copper(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(8960.);
    }

    public function diesel(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity((800. + 880.) / 2);
    }

    public function dural(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(2800.);
    }

    public function ethanol(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(789.3);
    }

    public function glass(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity((2400. + 2800.) / 2);
    }

    public function gold(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(19320.);
    }

    public function humanBodyAvg(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(985.);
    }

    public function hydrogen(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(0.08895);
    }

    public function hydrogen700bar(): IDensity
    {
        // https://energies.airliquide.com/resources-planet-hydrogen/how-hydrogen-stored
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(42.);
    }

    public function hydrogenLiquid(): IDensity
    {
        // https://energies.airliquide.com/resources-planet-hydrogen/how-hydrogen-stored
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(71.);
    }

    /**
     * 0 °C
     */
    public function ice(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(916.8);
    }

    public function iron(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(7870.);
    }

    public function lead(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(11340.);
    }

    public function lpg(): IDensity
    {
        // http://www.cng4you.cz/cng-info/vyhody-cng.html
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity((502. + 579.) / 2);
    }

    public function mercury(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(13579.04);
    }

    public function methanol(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(791.7);
    }

    public function paper(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity((700. + 1100.) / 2);
    }

    public function petroleum(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity((730. + 1000.) / 2);
    }

    public function platina(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(21450.);
    }

    public function salt(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(2160.);
    }

    /**
     * 13 °C
     */
    public function seaWater(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(1024.);
    }

    public function silver(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(10500.);
    }

    public function steel(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(7850.);
    }

    public function titan(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(4540.);
    }

    public function water(): IDensity
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->densities[__FUNCTION__] ??= $this->makeDensity(998.);
    }

    private function makeDensity(float $value): IDensity
    {
        return $this->quantityFactory->createWithValue(IDensity::class, $value);
    }
}
