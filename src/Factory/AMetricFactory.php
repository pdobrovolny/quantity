<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory;

use DI\FactoryInterface;
use Ds\Map;
use PDobrovolny\Quantity\Contracts\Enums\IMetricExponent;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IQuantityMetricFactory;
use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Enums\EDimensions;
use PDobrovolny\Quantity\Enums\EMetricExponent;

/**
 * @template TQuantity of IMetric
 *
 * @template-implements  IQuantityMetricFactory<TQuantity>
 */
abstract readonly class AMetricFactory implements IQuantityMetricFactory
{
    /** @var class-string<TQuantity> */
    public string $class;
    public EDimensions $dimensions;
    /** @var Map<string,IMetricExponent> */
    private Map $cache;

    /**
     * @param class-string<TQuantity> $interface
     */
    public function __construct(
        FactoryInterface $factory,
        public string $interface,
        private bool $allowNegative = false
    ) {
        $this->class = $factory->make($this->interface, ['value' => 0])::class;
        $this->dimensions = EDimensions::fromQuantity($this->interface);
        $this->cache = new Map([]);
    }

    #[\Override]
    final public function create(float $value, ?IMetricExponent $exponent = null): IMetric
    {
        \assert($value >= 0. || \is_nan($value) || $this->allowNegative);

        return $exponent === null
            ? new $this->class($value)
            : $this->makeQuantityExp($value, $exponent);
    }

    #[\Override]
    final public function makeQuantityDivide(float ...$values): IMetric
    {
        $initial = \array_shift($values);
        \assert(\is_float($initial));

        return new $this->class(
            \array_reduce(
                $values,
                static fn(float $reduced, float $value): float => $reduced / ($value ?: \NAN),
                $initial
            )
        );
    }

    #[\Override]
    final public function makeQuantityExp(
        float $mainValue,
        IMetricExponent $requestedExponent = EMetricExponent::BASE,
        IMetricExponent $mainExponent = EMetricExponent::BASE
    ): IMetric {
        \assert($mainValue >= 0. || \is_nan($mainValue) || $this->allowNegative);

        return new $this->class(
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
            ($this->cache[$requestedExponent->value . ':' . $mainExponent->value] ??= EMetricExponent::from(
            /** @phpstan-ignore binaryOp.invalid */
                $requestedExponent->value - $mainExponent->value
            ))
                ->getValue($mainValue, $this->dimensions)
        );
    }

    #[\Override]
    final public function makeQuantityProduct(float ...$values): IMetric
    {
        return new $this->class(\array_product($values));
    }
}
