<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ISquareLeagueFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ISquareLeague;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ISquareLeague>
 */
final readonly class SquareLeagueFactory extends AImperialFactory implements ISquareLeagueFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ISquareLeague::class);
    }
}
