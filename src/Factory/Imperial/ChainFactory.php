<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IChainFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IChain;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFurlong;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IYard;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IChain>
 */
final readonly class ChainFactory extends AImperialFactory implements IChainFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IChain::class);
    }

    #[\Override]
    public function createByFurlong(IFurlong $furlong): IChain
    {
        return $this->create(self::CHAIN_PER_FURLONG * $furlong->value);
    }

    #[\Override]
    public function createByLength(ILength $length): IChain
    {
        return $this->create($length->value / self::METRIC_CHAIN);
    }

    #[\Override]
    public function createByYard(IYard $yard): IChain
    {
        return $this->create($yard->value / self::YARD_PER_CHAIN);
    }
}
