<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ICubicLeagueFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ICubicLeague;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ICubicLeague>
 */
final readonly class CubicLeagueFactory extends AImperialFactory implements ICubicLeagueFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ICubicLeague::class);
    }
}
