<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ICubicInchFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ICubicInch;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ICubicInch>
 */
final readonly class CubicInchFactory extends AImperialFactory implements ICubicInchFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ICubicInch::class);
    }
}
