<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IPoundFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IPound;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IStone;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IPound>
 */
final readonly class PoundFactory extends AImperialFactory implements IPoundFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IPound::class);
    }

    #[\Override]
    public function createByMass(IMass $mass): IPound
    {
        return $this->create($mass->value / self::METRIC_POUND);
    }

    #[\Override]
    public function createByStone(IStone $stone): IPound
    {
        return $this->create($stone->value * self::POUND_PER_STONE);
    }
}
