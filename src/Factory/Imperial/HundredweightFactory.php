<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IHundredweightFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IHundredweight;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IStone;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ITon;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IHundredweight>
 */
final readonly class HundredweightFactory extends AImperialFactory implements IHundredweightFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IHundredweight::class);
    }

    #[\Override]
    public function createByMass(IMass $mass): IHundredweight
    {
        return $this->create($mass->value / self::METRIC_HW);
    }

    #[\Override]
    public function createByStone(IStone $stone): IHundredweight
    {
        return $this->create($stone->value / self::STONES_PER_HUNDREDWEIGHT);
    }

    #[\Override]
    public function createByTon(ITon $ton): IHundredweight
    {
        return $this->create($ton->value * self::HUNDREDWEIGHT_PER_TON);
    }
}
