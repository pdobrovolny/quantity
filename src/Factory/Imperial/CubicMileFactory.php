<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ICubicMileFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ICubicMile;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ICubicMile>
 */
final readonly class CubicMileFactory extends AImperialFactory implements ICubicMileFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ICubicMile::class);
    }
}
