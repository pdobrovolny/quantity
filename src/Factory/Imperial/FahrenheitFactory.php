<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IFahrenheitFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFahrenheit;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IThermodynamicTemperature;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IFahrenheit>
 */
final readonly class FahrenheitFactory extends AImperialFactory implements IFahrenheitFactory
{
    public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IFahrenheit::class, true);
    }

    #[\Override]
    public function createByThermodynamicTemperature(IThermodynamicTemperature $thermodynamicTemperature): IFahrenheit
    {
        return $this->create(1.8 * ($thermodynamicTemperature->value - self::KELVIN_CELSIUS) + 32);
    }
}
