<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IYardFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IChain;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFoot;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IYard;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IYard>
 */
final readonly class YardFactory extends AImperialFactory implements IYardFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IYard::class);
    }

    #[\Override]
    public function createByChain(IChain $chain): IYard
    {
        return $this->create(self::YARD_PER_CHAIN * $chain->value);
    }

    #[\Override]
    public function createByFoot(IFoot $foot): IYard
    {
        return $this->create($foot->value / self::FOOT_PER_YARD);
    }

    #[\Override]
    public function createByLength(ILength $length): IYard
    {
        return $this->create($length->value / self::METRIC_YARD);
    }

    #[\Override]
    public function createByMile(IMile $mile): IYard
    {
        return $this->create($mile->value * self::YARD_PER_MILE);
    }
}
