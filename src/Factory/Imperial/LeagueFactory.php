<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ILeagueFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ILeague;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ILeague>
 */
final readonly class LeagueFactory extends AImperialFactory implements ILeagueFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ILeague::class);
    }

    #[\Override]
    public function createByLength(ILength $length): ILeague
    {
        return $this->create($length->value / self::METRIC_LEA);
    }

    #[\Override]
    public function createByMile(IMile $mile): ILeague
    {
        return $this->create($mile->value / self::LEAGUE_PER_MILE);
    }
}
