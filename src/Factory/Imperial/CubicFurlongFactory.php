<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ICubicFurlongFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ICubicFurlong;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ICubicFurlong>
 */
final readonly class CubicFurlongFactory extends AImperialFactory implements ICubicFurlongFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ICubicFurlong::class);
    }
}
