<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial\UK;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\UK\IPintFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IFluidOunce;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGill;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IQuart;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IPint>
 */
final readonly class PintFactory extends AImperialFactory implements IPintFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IPint::class);
    }

    #[\Override]
    public function createByFluidOunceUK(IFluidOunce $fluidOunce): IPint
    {
        return $this->create($fluidOunce->value / self::OUNCE_PER_PINT);
    }

    #[\Override]
    public function createByGillUK(IGill $gill): IPint
    {
        return $this->create($gill->value * self::PINT_PER_GILL);
    }

    #[\Override]
    public function createByQuartUK(IQuart $quart): IPint
    {
        return $this->create($quart->value * self::PINT_PER_QUART);
    }

    #[\Override]
    public function createByVolume(IVolume $volume): IPint
    {
        return $this->create($volume->value / self::METRIC_PINT_UK);
    }
}
