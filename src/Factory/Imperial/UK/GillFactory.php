<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial\UK;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\UK\IGillFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGill;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IGill>
 */
final readonly class GillFactory extends AImperialFactory implements IGillFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IGill::class);
    }

    #[\Override]
    public function createByPintUK(IPint $pint): IGill
    {
        return $this->create($pint->value / self::PINT_PER_GILL);
    }

    #[\Override]
    public function createByVolume(IVolume $volume): IGill
    {
        return $this->create($volume->value / self::METRIC_GILL_UK);
    }
}
