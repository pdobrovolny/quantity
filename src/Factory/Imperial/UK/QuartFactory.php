<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial\UK;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\UK\IQuartFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGallon;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IQuart;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IQuart>
 */
final readonly class QuartFactory extends AImperialFactory implements IQuartFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IQuart::class);
    }

    #[\Override]
    public function createByGallonUK(IGallon $gallon): IQuart
    {
        return $this->create($gallon->value * self::QUART_PER_GALLON);
    }

    #[\Override]
    public function createByPintUK(IPint $pint): IQuart
    {
        return $this->create($pint->value / self::PINT_PER_QUART);
    }

    #[\Override]
    public function createByVolume(IVolume $volume): IQuart
    {
        return $this->create($volume->value / self::METRIC_QUART_UK);
    }
}
