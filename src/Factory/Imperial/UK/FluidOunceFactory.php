<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial\UK;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\UK\IFluidOunceFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IFluidOunce;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IFluidOunce>
 */
final readonly class FluidOunceFactory extends AImperialFactory implements IFluidOunceFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IFluidOunce::class);
    }

    #[\Override]
    public function createByPint(IPint $pint): IFluidOunce
    {
        return $this->create($pint->value * self::OUNCE_PER_PINT);
    }

    #[\Override]
    public function createByVolume(IVolume $volume): IFluidOunce
    {
        return $this->create($volume->value / self::METRIC_FL_OZ_UK);
    }
}
