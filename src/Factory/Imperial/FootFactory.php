<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IFootFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFoot;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IInch;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IYard;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IFoot>
 */
final readonly class FootFactory extends AImperialFactory implements IFootFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IFoot::class);
    }

    #[\Override]
    public function inch(IInch $inch): IFoot
    {
        return $this->create($inch->value / self::INCH_PER_FOOT);
    }

    #[\Override]
    public function length(ILength $length): IFoot
    {
        return $this->create($length->value / self::METRIC_FOOT);
    }

    #[\Override]
    public function mile(IMile $mile): IFoot
    {
        return $this->create($mile->value * self::FOOT_PER_MILE);
    }

    #[\Override]
    public function yard(IYard $yard): IFoot
    {
        return $this->create($yard->value * self::FOOT_PER_YARD);
    }
}
