<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IFurlongFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IChain;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFurlong;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IFurlong>
 */
final readonly class FurlongFactory extends AImperialFactory implements IFurlongFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IFurlong::class);
    }

    #[\Override]
    public function createByChain(IChain $chain): IFurlong
    {
        return $this->create($chain->value / self::CHAIN_PER_FURLONG);
    }

    #[\Override]
    public function createByLength(ILength $length): IFurlong
    {
        return $this->create($length->value / self::METRIC_FUR);
    }

    #[\Override]
    public function createByMile(IMile $mile): IFurlong
    {
        return $this->create($mile->value * self::FURLONG_PER_MILE);
    }
}
