<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ISquareInchFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ISquareInch;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ISquareInch>
 */
final readonly class SquareInchFactory extends AImperialFactory implements ISquareInchFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ISquareInch::class);
    }
}
