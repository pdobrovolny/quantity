<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ISquareChainFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ISquareChain;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ISquareChain>
 */
final readonly class SquareChainFactory extends AImperialFactory implements ISquareChainFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ISquareChain::class);
    }
}
