<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IMileFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFoot;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFurlong;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ILeague;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IYard;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IMile>
 */
final readonly class MileFactory extends AImperialFactory implements IMileFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IMile::class);
    }

    #[\Override]
    public function createByFoot(IFoot $foot): IMile
    {
        return $this->create($foot->value / self::FOOT_PER_MILE);
    }

    #[\Override]
    public function createByFurlong(IFurlong $furlong): IMile
    {
        return $this->create($furlong->value / self::FURLONG_PER_MILE);
    }

    #[\Override]
    public function createByLeague(ILeague $league): IMile
    {
        return $this->create($league->value * self::LEAGUE_PER_MILE);
    }

    #[\Override]
    public function createByLength(ILength $length): IMile
    {
        return $this->create($length->value / self::METRIC_MILE);
    }

    #[\Override]
    public function createByYard(IYard $yard): IMile
    {
        return $this->create($yard->value / self::YARD_PER_MILE);
    }
}
