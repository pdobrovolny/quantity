<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IInchFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFoot;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IInch;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IInch>
 */
final readonly class InchFactory extends AImperialFactory implements IInchFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IInch::class);
    }

    #[\Override]
    public function createByFoot(IFoot $foot): IInch
    {
        return $this->create($foot->value * self::INCH_PER_FOOT);
    }

    #[\Override]
    public function createByLength(ILength $length): IInch
    {
        return $this->create($length->value / self::METRIC_INCH);
    }
}
