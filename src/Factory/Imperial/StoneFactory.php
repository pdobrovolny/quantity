<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IStoneFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IHundredweight;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IPound;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IStone;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IStone>
 */
final readonly class StoneFactory extends AImperialFactory implements IStoneFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IStone::class);
    }

    #[\Override]
    public function createByHundredweight(IHundredweight $hundredweight): IStone
    {
        return $this->create($hundredweight->value * self::STONES_PER_HUNDREDWEIGHT);
    }

    #[\Override]
    public function createByMass(IMass $mass): IStone
    {
        return $this->create($mass->value / self::METRIC_STONE);
    }

    #[\Override]
    public function createByPound(IPound $pound): IStone
    {
        return $this->create($pound->value / self::POUNDS_PER_STONE);
    }
}
