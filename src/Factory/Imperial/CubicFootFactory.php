<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ICubicFootFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ICubicFoot;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ICubicFoot>
 */
final readonly class CubicFootFactory extends AImperialFactory implements ICubicFootFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ICubicFoot::class);
    }
}
