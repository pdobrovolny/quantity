<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ICubicYardFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ICubicYard;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ICubicYard>
 */
final readonly class CubicYardFactory extends AImperialFactory implements ICubicYardFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ICubicYard::class);
    }
}
