<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ISquareFootFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ISquareFoot;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ISquareFoot>
 */
final readonly class SquareFootFactory extends AImperialFactory implements ISquareFootFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ISquareFoot::class);
    }
}
