<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ITonFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IHundredweight;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ITon;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ITon>
 */
final readonly class TonFactory extends AImperialFactory implements ITonFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ITon::class);
    }

    #[\Override]
    public function createByHundredweight(IHundredweight $hundredweight): ITon
    {
        return $this->create($hundredweight->value / self::HUNDREDWEIGHT_PER_TON);
    }

    #[\Override]
    public function createByMass(IMass $mass): ITon
    {
        return $this->create($mass->value / self::METRIC_TON);
    }
}
