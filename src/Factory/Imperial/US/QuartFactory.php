<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial\US;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\US\IQuartFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGallon;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IQuart;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IQuart>
 */
final readonly class QuartFactory extends AImperialFactory implements IQuartFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IQuart::class);
    }

    #[\Override]
    public function createByGallonUS(IGallon $gallon): IQuart
    {
        return $this->create($gallon->value * self::QUART_PER_GALLON);
    }

    #[\Override]
    public function createByPintUS(IPint $pint): IQuart
    {
        return $this->create($pint->value / self::PINT_PER_QUART);
    }

    #[\Override]
    public function createByVolume(IVolume $volume): IQuart
    {
        return $this->create($volume->value / self::METRIC_QUART_US);
    }
}
