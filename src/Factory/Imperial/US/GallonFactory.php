<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial\US;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\US\IGallonFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGallon;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IQuart;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IGallon>
 */
final readonly class GallonFactory extends AImperialFactory implements IGallonFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IGallon::class);
    }

    #[\Override]
    public function createByQuartUS(IQuart $quart): IGallon
    {
        return $this->create($quart->value / self::QUART_PER_GALLON);
    }

    #[\Override]
    public function createByVolume(IVolume $volume): IGallon
    {
        return $this->create($volume->value / self::METRIC_GALLON_US);
    }
}
