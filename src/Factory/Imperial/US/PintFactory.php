<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial\US;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\US\IPintFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IFluidOunce;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGill;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IPint;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IQuart;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IPint>
 */
final readonly class PintFactory extends AImperialFactory implements IPintFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IPint::class);
    }

    #[\Override]
    public function fluidOunceUS(IFluidOunce $fluidOunce): IPint
    {
        return $this->create($fluidOunce->value / self::OUNCE_PER_PINT);
    }

    #[\Override]
    public function gillUS(IGill $gill): IPint
    {
        return $this->create(self::PINT_PER_GILL * $gill->value);
    }

    #[\Override]
    public function quartUS(IQuart $quart): IPint
    {
        return $this->create($quart->value * self::PINT_PER_QUART);
    }

    #[\Override]
    public function volume(IVolume $volume): IPint
    {
        return $this->create($volume->value / self::METRIC_PINT_US);
    }
}
