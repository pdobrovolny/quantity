<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ICubicChainFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ICubicChain;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ICubicChain>
 */
final readonly class CubicChainFactory extends AImperialFactory implements ICubicChainFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ICubicChain::class);
    }
}
