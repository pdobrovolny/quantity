<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IGrainFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IGrain;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<IGrain>
 */
final readonly class GrainFactory extends AImperialFactory implements IGrainFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IGrain::class);
    }

    #[\Override]
    public function createByMass(IMass $mass): IGrain
    {
        return $this->create($mass->value / self::METRIC_GRAIN);
    }
}
