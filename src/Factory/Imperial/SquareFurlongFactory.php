<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ISquareFurlongFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ISquareFurlong;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ISquareFurlong>
 */
final readonly class SquareFurlongFactory extends AImperialFactory implements ISquareFurlongFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ISquareFurlong::class);
    }
}
