<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ISquareMileFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ISquareMile;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ISquareMile>
 */
final readonly class SquareMileFactory extends AImperialFactory implements ISquareMileFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ISquareMile::class);
    }
}
