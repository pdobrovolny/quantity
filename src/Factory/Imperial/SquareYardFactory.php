<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Imperial;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\ISquareYardFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ISquareYard;
use PDobrovolny\Quantity\Factory\AImperialFactory;

/**
 * @template-extends AImperialFactory<ISquareYard>
 */
final readonly class SquareYardFactory extends AImperialFactory implements ISquareYardFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ISquareYard::class);
    }
}
