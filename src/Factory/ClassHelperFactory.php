<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory;

use Ds\Map;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;

final readonly class ClassHelperFactory
{

    /** @var Map<string,mixed> */
    private Map $cache;

    public function __construct()
    {
        $this->cache = new Map([]);
    }

    /**
     * @param class-string<object> $class
     */
    public function getClassName(string $class): string
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->cache[$class] ??= $this->makeClassName($class);
    }

    /**
     * @param class-string<object> $class
     *
     * @return array<string,array<string,class-string<IQuantity>>>
     */
    public function getFactoryMap(string $class): array
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->cache[$class] ??= $this->makeFactoryMap($class);
    }

    /**
     * @param class-string<object> $class
     *
     * @return non-empty-string
     */
    public function getNs(string $class): string
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->cache[$class] ??= \implode('\\', \explode('\\', $class, -1)) . '\\';
    }

    /**
     * @return string|class-string
     */
    private static function makeMethodNames(?\ReflectionType $type): string
    {
        return match (true) {
            $type instanceof \ReflectionUnionType => '???',
            $type instanceof \ReflectionNamedType => $type->getName(),
            default => throw new \InvalidArgumentException('Unsupported type'),
        };
    }

    /**
     * @param \ReflectionMethod $method
     *
     * @return array<\ReflectionType|null>
     */
    private static function makeMethodTypes(\ReflectionMethod $method): array
    {
        return \array_map(
            static fn(\ReflectionParameter $parameter) => $parameter->getType(),
            $method->getParameters()
        );
    }

    /**
     * @param class-string $interface
     *
     * @return array<\ReflectionMethod>
     */
    private function getMethods(string $interface): array
    {
        $reflectionClass = new \ReflectionClass($interface);

        return \array_filter(
            $reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC),
            static fn(
                \ReflectionMethod $method
            ) => $method->getNumberOfParameters() > 0 && \str_starts_with($method->getName(), '_') === false
        );
    }

    /**
     * @param class-string $class
     */
    private function makeClassName(string $class): string
    {
        $explode = \explode($this->getNs($class), $class);

        return \end($explode);
    }

    /**
     * @param class-string $class
     *
     * @return array<string,array<string,string|class-string>>
     */
    private function makeFactoryMap(string $class): array
    {
        $result = [];
        foreach ($this->getMethods($class) as $method) {
            $result[$method->getName()] = \array_combine(
                \array_map(
                    static fn(\ReflectionParameter $parameter): string => $parameter->getName(),
                    $method->getParameters()
                ),
                \array_map(self::makeMethodNames(...), self::makeMethodTypes($method))
            );
        }

        return $result;
    }
}
