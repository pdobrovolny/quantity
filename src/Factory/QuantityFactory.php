<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory;

use DI\Container;
use DI\ContainerBuilder;
use DI\Definition\Helper\DefinitionHelper;
use Ds\Map;
use PDobrovolny\Quantity\Contracts\Enums\IMetricExponent;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IQuantityImperialFactory;
use PDobrovolny\Quantity\Contracts\Factory\IQuantityFactory;
use PDobrovolny\Quantity\Contracts\Factory\IQuantityMainFactory;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IQuantityMetricFactory;
use PDobrovolny\Quantity\Contracts\Quantity\IImperial;
use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\IImperialUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\IImperialUS;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;

use function DI\autowire;

final readonly class QuantityFactory implements IQuantityFactory
{
    private const string CLASS_PATH1 = '\\*\\*\\*';
    private const string CLASS_PATH2 = '\\*\\*';
    private const string CLASS_UNIT = self::NS_ROOT . 'Container' . self::CLASS_PATH1;
    private const string CONTRACT_FACTORY1 = self::NS_ROOT . 'Contracts\\Factory' . self::CONTRACT_PATH1;
    private const string CONTRACT_FACTORY2 = self::NS_ROOT . 'Contracts\\Factory' . self::CONTRACT_PATH2;
    private const string CONTRACT_PATH1 = '\\*\\*\\I*';
    private const string CONTRACT_PATH2 = '\\*\\I*';
    private const string CONTRACT_UNIT = self::NS_ROOT . 'Contracts\\Quantity' . self::CONTRACT_PATH1;
    private const string NS_ROOT = 'PDobrovolny\\Quantity\\';
    private const string SEPARATOR = ':';

    private Container $container;
    private ClassHelperFactory $helper;
    /** @var Map<string,mixed> */
    private Map $cache;

    /**
     * @param array<class-string<IQuantity>,DefinitionHelper> $customDefinitions
     */
    public function __construct(array $customDefinitions = [])
    {
        $this->container = new ContainerBuilder()->addDefinitions(
            [
                // units
                self::CONTRACT_UNIT => autowire(self::CLASS_UNIT),

                //factories
                self::CONTRACT_FACTORY2 => autowire(__NAMESPACE__ . self::CLASS_PATH2),
                self::CONTRACT_FACTORY1 => autowire(__NAMESPACE__ . self::CLASS_PATH1),
            ]
        )
            ->addDefinitions($customDefinitions)
            ->build();

        $this->cache = new Map([]);
        $this->helper = $this->container->get(ClassHelperFactory::class);
    }

    /** @inheritdoc */
    #[\Override]
    public function create(string $interface, array $parameters): IQuantity
    {
        $factory = $this->getMainFactory($interface);

        $function = $this->getFunction($factory, $parameters)
            ?? throw new \InvalidArgumentException(
                \sprintf(
                    'Unknown %s formula for arguments %s',
                    $factory::class,
                    \implode(', ', \array_keys($parameters))
                )
            );

        return $factory->{$function}(...$parameters);
    }

    #[\Override]
    public function createWithValue(string $interface, float $value, ?IMetricExponent $exponent = null): IQuantity
    {
        $factory = $this->getMainFactory($interface);

        return $factory instanceof IQuantityMetricFactory
            ? $factory->create($value, $exponent)
            : $factory->create($value);
    }

    /**
     * @param non-empty-array<int|string,float|IQuantity> $parameters
     *
     * @return non-empty-array<string,float|IQuantity>
     */
    public function makeAssociations(array $parameters): array
    {
        $result = \array_filter($parameters, \is_string(...), \ARRAY_FILTER_USE_KEY);
        foreach (\array_filter($parameters, \is_int(...), \ARRAY_FILTER_USE_KEY) as $value) {
            $key = match (true) {
                \is_float($value) => 'value',
                /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
                \is_object($value) => $this->cache[__FUNCTION__ . ':' . $value::class] ??= \lcfirst(
                    $this->helper->getClassName($value::class)
                ),
            };
            \assert(\is_string($key));

            $result[$key] ??= $value;
        }

        /** @phpstan-ignore return.type */
        return $result;
    }

    /**
     * @param array<string,array<string,class-string<IQuantity>>> $map
     * @param array<string,float|object> $parameters
     *
     * @return array<string,array<string,class-string<IQuantity>>>
     */
    private static function filterFunctionsByType(array $map, array $parameters): array
    {
        return \array_filter(
            $map,
            static fn(array $data) => \count($data) === \count(
                    \array_filter(
                        $data,
                        static fn(
                            string $interface,
                            string $property
                        ): bool => $parameters[$property] instanceof $interface,
                        \ARRAY_FILTER_USE_BOTH
                    )
                )
        );
    }

    /**
     * @param IQuantityMainFactory<IQuantity> $factory
     * @param array<string,float|object> $associations
     */
    private function findFunction(IQuantityMainFactory $factory, array $associations): ?string
    {
        $possibleFunctions = $this->getPossibleFunctions($factory::class, $associations);

        return match (\count($possibleFunctions)) {
            0 => null,
            1 => \key($possibleFunctions),
            default => \key(self::filterFunctionsByType($possibleFunctions, $associations))
        };
    }

    /**
     * @param class-string<IQuantity> $class
     */
    private function getFactoryNs(string $class): string
    {
        return $this->helper->getNs(
                match (true) {
                    \is_subclass_of($class, IMetric::class) => IQuantityMetricFactory::class,
                    \is_subclass_of($class, IImperialUK::class),
                    \is_subclass_of($class, IImperialUS::class),
                    \is_subclass_of($class, IImperial::class) => IQuantityImperialFactory::class,
                    default => throw new \InvalidArgumentException(\sprintf('Factory not found: %s', $class))
                }
            ) . match (true) {
                \is_subclass_of($class, IImperialUK::class) => 'UK\\',
                \is_subclass_of($class, IImperialUS::class) => 'US\\',
                default => ''
            };
    }

    /**
     * @param IQuantityMainFactory<IQuantity> $factory
     * @param array<string,float|object> $associations
     */
    private function getFunction(IQuantityMainFactory $factory, array $associations): ?string
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->cache[\implode(self::SEPARATOR, [__FUNCTION__, ...\array_keys($associations)])]
            ??= $this->findFunction($factory, $associations);
    }

    /**
     * @template T of IQuantity
     *
     * @param class-string<T> $class
     *
     * @return IQuantityMainFactory<T>
     */
    private function getMainFactory(string $class): IQuantityMainFactory
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->cache[__FUNCTION__ . self::SEPARATOR . $class] ??= $this->container->get(
            $this->makeMainFactoryInterface($class)
        );
    }

    /**
     * @param class-string<IQuantityMainFactory<IQuantity>> $factoryClass
     * @param array<string,float|object> $parameters
     *
     * @return array<string,array<string,class-string<IQuantity>>>
     */
    private function getPossibleFunctions(string $factoryClass, array $parameters): array
    {
        $argumentKeys = \array_keys($parameters);

        return \array_filter(
            $this->helper->getFactoryMap($factoryClass),
            static fn(array $arguments) => \count(
                    \array_intersect(\array_keys($arguments), $argumentKeys)
                ) === \count($arguments)
        );
    }

    /**
     * @template T of IQuantity
     *
     * @param class-string<T> $class
     *
     * @return class-string<IQuantityMainFactory<T>>
     */
    private function makeMainFactoryInterface(string $class): string
    {
        /** @phpstan-ignore property.readOnlyAssignNotInConstructor */
        return $this->cache[__FUNCTION__ . self::SEPARATOR . $class] ??= $this->getFactoryNs($class)
            . 'I' . (\interface_exists($class)
                ? \substr($this->helper->getClassName($class), 1)
                : $this->helper->getClassName($class))
            . 'Factory';
    }
}
