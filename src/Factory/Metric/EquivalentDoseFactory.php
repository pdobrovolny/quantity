<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IEquivalentDoseFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IEquivalentDose;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IEquivalentDose>
 */
final readonly class EquivalentDoseFactory extends AMetricFactory implements IEquivalentDoseFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IEquivalentDose::class);
    }

    #[\Override]
    public function createByMassWork(IMass $mass, IWork $work): IEquivalentDose
    {
        return $this->makeQuantityDivide($work->value, $mass->value);
    }
}
