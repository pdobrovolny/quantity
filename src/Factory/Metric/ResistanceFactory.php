<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IResistanceFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IConductance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IResistance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IVoltAmpere;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IResistance>
 */
final readonly class ResistanceFactory extends AMetricFactory implements IResistanceFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IResistance::class);
    }

    #[\Override]
    public function createByConductance(IConductance $conductance): IResistance
    {
        return $this->makeQuantityDivide(1, $conductance->value);
    }

    #[\Override]
    public function createByCurrentVoltAmpere(IElectricCurrent $electricCurrent, IVoltAmpere $voltAmpere): IResistance
    {
        return $this->makeQuantityDivide($voltAmpere->value, $electricCurrent->value ** 2);
    }

    #[\Override]
    public function createByCurrentVoltage(IElectricCurrent $electricCurrent, IVoltage $voltage): IResistance
    {
        return $this->makeQuantityDivide($voltage->value, $electricCurrent->value);
    }

    #[\Override]
    public function createByVoltageVoltAmpere(IVoltage $voltage, IVoltAmpere $voltAmpere): IResistance
    {
        return $this->makeQuantityDivide($voltage->value ** 2, $voltAmpere->value);
    }
}
