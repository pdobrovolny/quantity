<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\ILuminousFluxFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILuminousIntensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IIlluminance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ILuminousFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ISolidAngle;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<ILuminousFlux>
 */
final readonly class LuminousFluxFactory extends AMetricFactory implements ILuminousFluxFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ILuminousFlux::class);
    }

    #[\Override]
    public function createByAreaIlluminance(IArea $area, IIlluminance $illuminance): ILuminousFlux
    {
        return $this->makeQuantityProduct($illuminance->value, $area->value);
    }

    #[\Override]
    public function createByLuminousIntensitySolidAngle(
        ILuminousIntensity $luminousIntensity,
        ISolidAngle $solidAngle
    ): ILuminousFlux {
        return $this->makeQuantityProduct($luminousIntensity->value, $solidAngle->value);
    }
}
