<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IVoltageFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICapacitance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICharge;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IConductance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IResistance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IVoltAmpere;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IVoltage>
 */
final readonly class VoltageFactory extends AMetricFactory implements IVoltageFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IVoltage::class);
    }

    #[\Override]
    public function createByCapacitanceCharge(ICapacitance $capacitance, ICharge $charge): IVoltage
    {
        return $this->makeQuantityDivide($charge->value, $capacitance->value);
    }

    #[\Override]
    public function createByChargeWork(ICharge $charge, IWork $work): IVoltage
    {
        return $this->makeQuantityDivide($work->value, $charge->value);
    }

    #[\Override]
    public function createByCurrentConductance(IElectricCurrent $electricCurrent, IConductance $conductance): IVoltage
    {
        return $this->makeQuantityDivide($electricCurrent->value, $conductance->value);
    }

    #[\Override]
    public function createByCurrentPower(IElectricCurrent $electricCurrent, IPower $power): IVoltage
    {
        return $this->makeQuantityDivide($power->value, $electricCurrent->value);
    }

    #[\Override]
    public function createByCurrentResistance(IElectricCurrent $electricCurrent, IResistance $resistance): IVoltage
    {
        return $this->makeQuantityProduct($resistance->value, $electricCurrent->value);
    }

    #[\Override]
    public function createByCurrentVoltAmpere(IElectricCurrent $electricCurrent, IVoltAmpere $voltAmpere): IVoltage
    {
        return $this->makeQuantityDivide($voltAmpere->value, $electricCurrent->value);
    }

    #[\Override]
    public function createByResistanceVoltAmpere(IResistance $resistance, IVoltAmpere $voltAmpere): IVoltage
    {
        return $this->create(\sqrt($voltAmpere->value * $resistance->value));
    }

    #[\Override]
    public function createByTimeCharge(ITime $time, ICharge $charge): IVoltage
    {
        return $this->makeQuantityDivide($time->value, $charge->value);
    }

    #[\Override]
    public function createByTimeMagneticFlux(ITime $time, IMagneticFlux $magneticFlux): IVoltage
    {
        return $this->makeQuantityDivide($magneticFlux->value, $time->value);
    }
}
