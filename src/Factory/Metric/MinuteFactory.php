<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IMinuteFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IMinute;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IMinute>
 */
final readonly class MinuteFactory extends AMetricFactory implements IMinuteFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IMinute::class);
    }

    #[\Override]
    public function createByTime(ITime $time): IMinute
    {
        return $this->makeQuantityDivide($time->value, self::SEC_PER_MINUTE);
    }
}
