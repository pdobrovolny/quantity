<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IInductanceFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IInductance;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IInductance>
 */
final readonly class InductanceFactory extends AMetricFactory implements IInductanceFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IInductance::class);
    }
}
