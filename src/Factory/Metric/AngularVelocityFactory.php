<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IAngularVelocityFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngle;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngularVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IFrequency;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IAngularVelocity>
 */
final readonly class AngularVelocityFactory extends AMetricFactory implements IAngularVelocityFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IAngularVelocity::class);
    }

    #[\Override]
    public function createByFrequency(IFrequency $frequency): IAngularVelocity
    {
        return $this->makeQuantityProduct($frequency->value, self::ANGLE_MAX);
    }

    #[\Override]
    public function createByTimeAngle(ITime $time, IAngle $angle): IAngularVelocity
    {
        return $this->makeQuantityDivide($angle->value, $time->value);
    }
}
