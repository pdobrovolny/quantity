<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\ILiterFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ILiter;
use PDobrovolny\Quantity\Enums\EMetricExponent;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<ILiter>
 */
final readonly class LiterFactory extends AMetricFactory implements ILiterFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ILiter::class);
    }

    #[\Override]
    public function createByVolume(IVolume $volume): ILiter
    {
        return $this->makeQuantityExp($volume->value, EMetricExponent::KILO);
    }
}
