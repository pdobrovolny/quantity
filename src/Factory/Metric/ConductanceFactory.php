<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IConductanceFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IConductance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IResistance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IConductance>
 */
final readonly class ConductanceFactory extends AMetricFactory implements IConductanceFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IConductance::class);
    }

    #[\Override]
    public function createByCurrentVoltage(IElectricCurrent $electricCurrent, IVoltage $voltage): IConductance
    {
        return $this->makeQuantityDivide($electricCurrent->value, $voltage->value);
    }

    #[\Override]
    public function createByResistance(IResistance $resistance): IConductance
    {
        return $this->makeQuantityDivide(1, $resistance->value);
    }
}
