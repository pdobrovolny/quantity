<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IDegreeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngle;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcMinute;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcSecond;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDegree;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IDegree>
 */
final readonly class DegreeFactory extends AMetricFactory implements IDegreeFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IDegree::class);
    }

    #[\Override]
    public function createByAngle(IAngle $angle): IDegree
    {
        $value = $angle->value;

        return $this->create($value === 0. ? 0. : self::DEGREE_MAX / (self::ANGLE_MAX / $value));
    }

    #[\Override]
    public function createByArcMinute(IArcMinute $arcMinute): IDegree
    {
        return $this->makeQuantityDivide($arcMinute->value, self::DEGREE_SUB_UNIT);
    }

    #[\Override]
    public function createByArcSecond(IArcSecond $arcSecond): IDegree
    {
        return $this->makeQuantityDivide($arcSecond->value, self::DEGREE_SUB_UNIT ** 2);
    }
}
