<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IMassFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IGrain;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IHundredweight;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IPound;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IStone;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ITon;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAbsorbedDose;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IEquivalentDose;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IAtomicMassUnit;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ITonne;
use PDobrovolny\Quantity\Enums\EMetricExponent;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IMass>
 */
final readonly class MassFactory extends AMetricFactory implements IMassFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IMass::class);
    }

    #[\Override]
    public function createByAbsorbedDoseWork(IAbsorbedDose $absorbedDose, IWork $work): IMass
    {
        return $this->makeQuantityDivide($work->value, $absorbedDose->value);
    }

    #[\Override]
    public function createByAccelerationForce(IAcceleration $acceleration, IForce $force): IMass
    {
        return $this->makeQuantityDivide($force->value, $acceleration->value);
    }

    #[\Override]
    public function createByAtomicMassUnit(IAtomicMassUnit $atomicMassUnit): IMass
    {
        return $this->makeQuantityProduct($atomicMassUnit->value, self::ATOMIC_MASS);
    }

    #[\Override]
    public function createByEquivalentDoseWork(IEquivalentDose $equivalentDose, IWork $work): IMass
    {
        return $this->makeQuantityDivide($work->value, $equivalentDose->value);
    }

    #[\Override]
    public function createByGrain(IGrain $grain): IMass
    {
        return $this->makeQuantityProduct($grain->value, self::METRIC_GRAIN);
    }

    #[\Override]
    public function createByHundredweight(IHundredweight $hundredweight): IMass
    {
        return $this->makeQuantityProduct($hundredweight->value, self::METRIC_HW);
    }

    #[\Override]
    public function createByLengthAccelerationWork(ILength $length, IAcceleration $acceleration, IWork $work): IMass
    {
        return $this->makeQuantityDivide($work->value, $length->value * $acceleration->value);
    }

    #[\Override]
    public function createByPound(IPound $pound): IMass
    {
        return $this->makeQuantityProduct($pound->value, self::METRIC_POUND);
    }

    #[\Override]
    public function createByStone(IStone $stone): IMass
    {
        return $this->makeQuantityProduct($stone->value, self::METRIC_STONE);
    }

    #[\Override]
    public function createByTon(ITon $ton): IMass
    {
        return $this->makeQuantityProduct($ton->value, self::METRIC_TON);
    }

    #[\Override]
    public function createByTonne(ITonne $tonne): IMass
    {
        return $this->makeQuantityExp($tonne->value, EMetricExponent::KILO);
    }

    #[\Override]
    public function createByVelocityWork(IVelocity $velocity, IWork $work): IMass
    {
        return $this->makeQuantityDivide(2 * $work->value, $velocity->value ** 2);
    }

    #[\Override]
    public function createByVolumeDensity(IVolume $volume, IDensity $density): IMass
    {
        return $this->makeQuantityProduct($volume->value, $density->value);
    }
}
