<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IVolumeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IFluidOunce as IFluidOunceUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGallon as IGallonUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGill as IGillUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IPint as IPintUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IQuart as IQuartUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IFluidOunce as IFluidOunceUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGallon as IGallonUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGill as IGillUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IPint as IPintUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IQuart as IQuartUS;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolumetricFlowRate;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ILiter;
use PDobrovolny\Quantity\Enums\EMetricExponent;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IVolume>
 */
final readonly class VolumeFactory extends AMetricFactory implements IVolumeFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IVolume::class);
    }

    #[\Override]
    public function createByFluidOunceUK(IFluidOunceUK $fluidOunce): IVolume
    {
        return $this->makeQuantityProduct($fluidOunce->value, self::METRIC_FL_OZ_UK);
    }

    #[\Override]
    public function createByFluidOunceUS(IFluidOunceUS $fluidOunce): IVolume
    {
        return $this->makeQuantityProduct($fluidOunce->value, self::METRIC_FL_OZ_US);
    }

    #[\Override]
    public function createByGallonUK(IGallonUK $gallon): IVolume
    {
        return $this->makeQuantityProduct($gallon->value, self::METRIC_GALLON_UK);
    }

    #[\Override]
    public function createByGallonUS(IGallonUS $gallon): IVolume
    {
        return $this->makeQuantityProduct($gallon->value, self::METRIC_GALLON_US);
    }

    #[\Override]
    public function createByGillUK(IGillUK $gill): IVolume
    {
        return $this->makeQuantityProduct($gill->value, self::METRIC_GILL_UK);
    }

    #[\Override]
    public function createByGillUS(IGillUS $gill): IVolume
    {
        return $this->makeQuantityProduct($gill->value, self::METRIC_GILL_US);
    }

    #[\Override]
    public function createByLiter(ILiter $liter): IVolume
    {
        return $this->makeQuantityExp($liter->value, EMetricExponent::DECI);
    }

    #[\Override]
    public function createByMassDensity(IMass $mass, IDensity $density): IVolume
    {
        return $this->makeQuantityDivide($mass->value, $density->value);
    }

    #[\Override]
    public function createByPintUK(IPintUK $pint): IVolume
    {
        return $this->makeQuantityProduct($pint->value, self::METRIC_PINT_UK);
    }

    #[\Override]
    public function createByPintUS(IPintUS $pint): IVolume
    {
        return $this->makeQuantityProduct($pint->value, self::METRIC_PINT_US);
    }

    #[\Override]
    public function createByPressureWork(IPressure $pressure, IWork $work): IVolume
    {
        return $this->makeQuantityDivide($work->value, $pressure->value);
    }

    #[\Override]
    public function createByQuartUK(IQuartUK $quart): IVolume
    {
        return $this->makeQuantityProduct($quart->value, self::METRIC_QUART_UK);
    }

    #[\Override]
    public function createByQuartUS(IQuartUS $quart): IVolume
    {
        return $this->makeQuantityProduct($quart->value, self::METRIC_QUART_US);
    }

    #[\Override]
    public function createByTimeVolumetricFlowRate(ITime $time, IVolumetricFlowRate $volumetricFlowRate): IVolume
    {
        return $this->makeQuantityProduct($volumetricFlowRate->value, $time->value);
    }
}
