<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IAmountOfSubstanceFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IAmountOfSubstance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICatalyticActivity;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IAmountOfSubstance>
 */
final readonly class AmountOfSubstanceFactory extends AMetricFactory implements IAmountOfSubstanceFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IAmountOfSubstance::class);
    }

    #[\Override]
    public function createByTimeCatalyticActivity(
        ITime $time,
        ICatalyticActivity $catalyticActivity
    ): IAmountOfSubstance {
        return $this->makeQuantityProduct($catalyticActivity->value, $time->value);
    }
}
