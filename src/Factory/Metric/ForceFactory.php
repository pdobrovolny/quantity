<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IForceFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IForce>
 */
final readonly class ForceFactory extends AMetricFactory implements IForceFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IForce::class);
    }

    #[\Override]
    public function createByAreaPressure(IArea $area, IPressure $pressure): IForce
    {
        return $this->makeQuantityProduct($area->value, $pressure->value);
    }

    #[\Override]
    public function createByAreaWork(IArea $area, IWork $work): IForce
    {
        return $this->makeQuantityProduct($work->value, $area->value);
    }

    #[\Override]
    public function createByLengthAccelerationAreaDensity(
        ILength $length,
        IAcceleration $acceleration,
        IArea $area,
        IDensity $density
    ): IForce {
        return $this->makeQuantityProduct($area->value, $length->value, $density->value, $acceleration->value);
    }

    #[\Override]
    public function createByLengthWork(ILength $length, IWork $work): IForce
    {
        return $this->makeQuantityDivide($work->value, $length->value);
    }

    #[\Override]
    public function createByMassAcceleration(IMass $mass, IAcceleration $acceleration): IForce
    {
        return $this->makeQuantityProduct($mass->value, $acceleration->value);
    }

    #[\Override]
    public function createByPowerVelocity(IPower $power, IVelocity $velocity): IForce
    {
        return $this->makeQuantityDivide($power->value, $velocity->value);
    }
}
