<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IPowerFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IPower>
 */
final readonly class PowerFactory extends AMetricFactory implements IPowerFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IPower::class);
    }

    #[\Override]
    public function createByCurrentVoltage(IElectricCurrent $electricCurrent, IVoltage $voltage): IPower
    {
        return $this->makeQuantityProduct($voltage->value, $electricCurrent->value);
    }

    #[\Override]
    public function createByForceVelocity(IForce $force, IVelocity $velocity): IPower
    {
        return $this->makeQuantityProduct($force->value, $velocity->value);
    }

    #[\Override]
    public function createByTimeWork(ITime $time, IWork $work): IPower
    {
        return $this->makeQuantityDivide($work->value, $time->value);
    }
}
