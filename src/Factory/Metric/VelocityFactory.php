<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IVelocityFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IVelocity>
 */
final readonly class VelocityFactory extends AMetricFactory implements IVelocityFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IVelocity::class);
    }

    #[\Override]
    public function createByForcePower(IForce $force, IPower $power): IVelocity
    {
        return $this->makeQuantityDivide($power->value, $force->value);
    }

    #[\Override]
    public function createByLengthTime(ILength $length, ITime $time): IVelocity
    {
        return $this->makeQuantityDivide($length->value, $time->value);
    }

    #[\Override]
    public function createByMassWork(IMass $mass, IWork $work): IVelocity
    {
        return $this->create(\sqrt(2 * $work->value / ($mass->value ?: \NAN)));
    }

    #[\Override]
    public function createByTimeAcceleration(ITime $time, IAcceleration $acceleration): IVelocity
    {
        return $this->makeQuantityProduct($acceleration->value, $time->value);
    }
}
