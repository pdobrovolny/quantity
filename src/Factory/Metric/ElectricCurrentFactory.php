<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IElectricCurrentFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICharge;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IConductance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IResistance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IVoltAmpere;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IElectricCurrent>
 */
final readonly class ElectricCurrentFactory extends AMetricFactory implements IElectricCurrentFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IElectricCurrent::class);
    }

    #[\Override]
    public function createByConductanceVoltage(IConductance $conductance, IVoltage $voltage): IElectricCurrent
    {
        return $this->makeQuantityProduct($voltage->value, $conductance->value);
    }

    #[\Override]
    public function createByPowerVoltage(IPower $power, IVoltage $voltage): IElectricCurrent
    {
        return $this->makeQuantityDivide($power->value, $voltage->value);
    }

    #[\Override]
    public function createByResistanceVoltAmpere(IResistance $resistance, IVoltAmpere $voltAmpere): IElectricCurrent
    {
        return $this->create(\sqrt($voltAmpere->value / ($resistance->value ?: \NAN)));
    }

    #[\Override]
    public function createByResistanceVoltage(IResistance $resistance, IVoltage $voltage): IElectricCurrent
    {
        return $this->makeQuantityDivide($voltage->value, $resistance->value);
    }

    #[\Override]
    public function createByTimeCharge(ITime $time, ICharge $charge): IElectricCurrent
    {
        return $this->makeQuantityDivide($charge->value, $time->value);
    }

    #[\Override]
    public function createByVoltageVoltAmpere(IVoltage $voltage, IVoltAmpere $voltAmpere): IElectricCurrent
    {
        return $this->makeQuantityDivide($voltAmpere->value, $voltage->value);
    }
}
