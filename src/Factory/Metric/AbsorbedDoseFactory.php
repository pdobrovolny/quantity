<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IAbsorbedDoseFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAbsorbedDose;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IAbsorbedDose>
 */
final readonly class AbsorbedDoseFactory extends AMetricFactory implements IAbsorbedDoseFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IAbsorbedDose::class);
    }

    #[\Override]
    public function createByMassWork(IMass $mass, IWork $work): IAbsorbedDose
    {
        return $this->makeQuantityDivide($work->value, $mass->value);
    }
}
