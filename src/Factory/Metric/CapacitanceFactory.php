<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\ICapacitanceFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICapacitance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICharge;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<ICapacitance>
 */
final readonly class CapacitanceFactory extends AMetricFactory implements ICapacitanceFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ICapacitance::class);
    }

    #[\Override]
    public function createByChargeVoltage(ICharge $charge, IVoltage $voltage): ICapacitance
    {
        return $this->makeQuantityDivide($charge->value, $voltage->value);
    }
}
