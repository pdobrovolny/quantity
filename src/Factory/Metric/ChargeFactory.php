<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IChargeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICapacitance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICharge;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<ICharge>
 */
final readonly class ChargeFactory extends AMetricFactory implements IChargeFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ICharge::class);
    }

    #[\Override]
    public function createByCapacitanceVoltage(ICapacitance $capacitance, IVoltage $voltage): ICharge
    {
        return $this->makeQuantityProduct($capacitance->value, $voltage->value);
    }

    #[\Override]
    public function createByCurrentTime(IElectricCurrent $electricCurrent, ITime $time): ICharge
    {
        return $this->makeQuantityProduct($electricCurrent->value, $time->value);
    }

    #[\Override]
    public function createByTimeVoltage(ITime $time, IVoltage $voltage): ICharge
    {
        return $this->makeQuantityDivide($time->value, $voltage->value);
    }

    #[\Override]
    public function createByVoltageWork(IVoltage $voltage, IWork $work): ICharge
    {
        return $this->makeQuantityDivide($work->value, $voltage->value);
    }
}
