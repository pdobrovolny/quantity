<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IFrequencyFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngularVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IFrequency;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IFrequency>
 */
final readonly class FrequencyFactory extends AMetricFactory implements IFrequencyFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IFrequency::class);
    }

    #[\Override]
    public function createByAngularVelocity(IAngularVelocity $angularVelocity): IFrequency
    {
        return $this->makeQuantityDivide($angularVelocity->value, self::ANGLE_MAX);
    }
}
