<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IAccelerationFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IAcceleration>
 */
final readonly class AccelerationFactory extends AMetricFactory implements IAccelerationFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IAcceleration::class, true);
    }

    #[\Override]
    public function createByLengthAreaForceDensity(
        ILength $length,
        IArea $area,
        IForce $force,
        IDensity $density
    ): IAcceleration {
        return $this->makeQuantityDivide($force->value, $area->value * $length->value * $density->value);
    }

    #[\Override]
    public function createByLengthMassWork(ILength $length, IMass $mass, IWork $work): IAcceleration
    {
        return $this->makeQuantityDivide($work->value, $mass->value * $length->value);
    }

    #[\Override]
    public function createByLengthPressureDensity(
        ILength $length,
        IPressure $pressure,
        IDensity $density
    ): IAcceleration {
        return $this->makeQuantityDivide($pressure->value, $density->value * $length->value);
    }

    #[\Override]
    public function createByMassForce(IMass $mass, IForce $force): IAcceleration
    {
        return $this->makeQuantityDivide($force->value, $mass->value);
    }

    #[\Override]
    public function createByTimeVelocity(ITime $time, IVelocity $velocity): IAcceleration
    {
        return $this->makeQuantityDivide($velocity->value, $time->value);
    }
}
