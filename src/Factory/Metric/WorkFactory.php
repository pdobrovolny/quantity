<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IWorkFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAbsorbedDose;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICharge;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IEquivalentDose;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IWork>
 */
final readonly class WorkFactory extends AMetricFactory implements IWorkFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IWork::class);
    }

    #[\Override]
    public function createByAreaForce(IArea $area, IForce $force): IWork
    {
        return $this->makeQuantityDivide($force->value, $area->value);
    }

    #[\Override]
    public function createByChargeVoltage(ICharge $charge, IVoltage $voltage): IWork
    {
        return $this->makeQuantityProduct($voltage->value, $charge->value);
    }

    #[\Override]
    public function createByLengthAreaPressure(ILength $length, IArea $area, IPressure $pressure): IWork
    {
        return $this->makeQuantityProduct($pressure->value, $area->value, $length->value);
    }

    #[\Override]
    public function createByLengthForce(ILength $length, IForce $force): IWork
    {
        return $this->makeQuantityProduct($force->value, $length->value);
    }

    #[\Override]
    public function createByLengthMassAcceleration(ILength $length, IMass $mass, IAcceleration $acceleration): IWork
    {
        return $this->makeQuantityProduct($mass->value, $length->value, $acceleration->value);
    }

    #[\Override]
    public function createByMassAbsorbedDose(IMass $mass, IAbsorbedDose $absorbedDose): IWork
    {
        return $this->makeQuantityProduct($absorbedDose->value, $mass->value);
    }

    #[\Override]
    public function createByMassEquivalentDose(IMass $mass, IEquivalentDose $equivalentDose): IWork
    {
        return $this->makeQuantityProduct($equivalentDose->value, $mass->value);
    }

    #[\Override]
    public function createByMassVelocity(IMass $mass, IVelocity $velocity): IWork
    {
        return $this->makeQuantityProduct(1 / 2, $mass->value, $velocity->value ** 2);
    }

    #[\Override]
    public function createByPressureVolume(IPressure $pressure, IVolume $volume): IWork
    {
        return $this->makeQuantityProduct($pressure->value, $volume->value);
    }

    #[\Override]
    public function createByTimePower(ITime $time, IPower $power): IWork
    {
        return $this->makeQuantityProduct($power->value, $time->value);
    }
}
