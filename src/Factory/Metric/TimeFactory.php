<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\ITimeFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IAmountOfSubstance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngle;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngularVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICatalyticActivity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICharge;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IRadioactivity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolumetricFlowRate;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDay;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IHour;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IMinute;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<ITime>
 */
final readonly class TimeFactory extends AMetricFactory implements ITimeFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ITime::class);
    }

    #[\Override]
    public function createByAccelerationVelocity(IAcceleration $acceleration, IVelocity $velocity): ITime
    {
        return $this->makeQuantityDivide($velocity->value, $acceleration->value);
    }

    #[\Override]
    public function createByAmountOfSubstanceCatalyticActivity(
        IAmountOfSubstance $amountOfSubstance,
        ICatalyticActivity $catalyticActivity
    ): ITime {
        return $this->makeQuantityDivide($amountOfSubstance->value, $catalyticActivity->value);
    }

    #[\Override]
    public function createByAngleAngularVelocity(IAngle $angle, IAngularVelocity $angularVelocity): ITime
    {
        return $this->makeQuantityDivide($angle->value, $angularVelocity->value);
    }

    #[\Override]
    public function createByChargeVoltage(ICharge $charge, IVoltage $voltage): ITime
    {
        return $this->makeQuantityProduct($voltage->value, $charge->value);
    }

    #[\Override]
    public function createByCurrentCharge(IElectricCurrent $electricCurrent, ICharge $charge): ITime
    {
        return $this->makeQuantityDivide($charge->value, $electricCurrent->value);
    }

    #[\Override]
    public function createByDay(IDay $day): ITime
    {
        return $this->makeQuantityProduct($day->value, self::SEC_PER_DAY);
    }

    #[\Override]
    public function createByHour(IHour $hour): ITime
    {
        return $this->makeQuantityProduct($hour->value, self::SEC_PER_HOUR);
    }

    #[\Override]
    public function createByLengthVelocity(ILength $length, IVelocity $velocity): ITime
    {
        return $this->makeQuantityDivide($length->value, $velocity->value);
    }

    #[\Override]
    public function createByMagneticFluxVoltage(IMagneticFlux $magneticFlux, IVoltage $voltage): ITime
    {
        return $this->makeQuantityDivide($magneticFlux->value, $voltage->value);
    }

    #[\Override]
    public function createByMinute(IMinute $minute): ITime
    {
        return $this->makeQuantityProduct($minute->value, self::SEC_PER_MINUTE);
    }

    #[\Override]
    public function createByPowerWork(IPower $power, IWork $work): ITime
    {
        return $this->makeQuantityDivide($work->value, $power->value);
    }

    #[\Override]
    public function createByRadioactivity(IRadioactivity $radioactivity): ITime
    {
        return $this->makeQuantityDivide(1, $radioactivity->value);
    }

    #[\Override]
    public function createByVolumeVolumetricFlowRate(IVolume $volume, IVolumetricFlowRate $volumetricFlowRate): ITime
    {
        return $this->makeQuantityDivide($volume->value, $volumetricFlowRate->value);
    }
}
