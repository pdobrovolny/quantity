<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IArcSecondFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcMinute;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcSecond;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDegree;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IArcSecond>
 */
final readonly class ArcSecondFactory extends AMetricFactory implements IArcSecondFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IArcSecond::class);
    }

    #[\Override]
    public function createByArcMinute(IArcMinute $arcMinute): IArcSecond
    {
        return $this->makeQuantityProduct($arcMinute->value, self::DEGREE_SUB_UNIT);
    }

    #[\Override]
    public function createByDegree(IDegree $degree): IArcSecond
    {
        return $this->makeQuantityProduct($degree->value, self::DEGREE_SUB_UNIT ** 2);
    }
}
