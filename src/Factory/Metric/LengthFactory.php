<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\ILengthFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IChain;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFoot;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFurlong;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IInch;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ILeague;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IYard;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IAstronomicalUnit;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ILightYear;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IOpticalPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IParsec;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<ILength>
 */
final readonly class LengthFactory extends AMetricFactory implements ILengthFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ILength::class);
    }

    #[\Override]
    public function createByAccelerationAreaForceDensity(
        IAcceleration $acceleration,
        IArea $area,
        IForce $force,
        IDensity $density
    ): ILength {
        return $this->makeQuantityDivide(
            $force->value,
            $area->value * $density->value * $acceleration->value
        );
    }

    #[\Override]
    public function createByAccelerationPressureDensity(
        IAcceleration $acceleration,
        IPressure $pressure,
        IDensity $density
    ): ILength {
        return $this->makeQuantityDivide($pressure->value, $density->value * $acceleration->value);
    }

    #[\Override]
    public function createByAreaPressureWork(IArea $area, IPressure $pressure, IWork $work): ILength
    {
        return $this->makeQuantityDivide($work->value, $pressure->value * $area->value);
    }

    #[\Override]
    public function createByAstronomicalUnit(IAstronomicalUnit $astronomicalUnit): ILength
    {
        return $this->makeQuantityProduct($astronomicalUnit->value, self::LENGTH_PER_ASTRO);
    }

    #[\Override]
    public function createByChain(IChain $chain): ILength
    {
        return $this->makeQuantityProduct($chain->value, self::METRIC_CHAIN);
    }

    #[\Override]
    public function createByFoot(IFoot $foot): ILength
    {
        return $this->makeQuantityProduct($foot->value, self::METRIC_FOOT);
    }

    #[\Override]
    public function createByForceWork(IForce $force, IWork $work): ILength
    {
        return $this->makeQuantityDivide($work->value, $force->value);
    }

    #[\Override]
    public function createByFurlong(IFurlong $furlong): ILength
    {
        return $this->makeQuantityProduct($furlong->value, self::METRIC_FUR);
    }

    #[\Override]
    public function createByInch(IInch $inch): ILength
    {
        return $this->makeQuantityProduct($inch->value, self::METRIC_INCH);
    }

    #[\Override]
    public function createByLeague(ILeague $league): ILength
    {
        return $this->makeQuantityProduct($league->value, self::METRIC_LEA);
    }

    #[\Override]
    public function createByLightYear(ILightYear $lightYear): ILength
    {
        return $this->makeQuantityProduct($lightYear->value, self::LENGTH_PER_LIGHT_YEAR);
    }

    #[\Override]
    public function createByMassAccelerationWork(IMass $mass, IAcceleration $acceleration, IWork $work): ILength
    {
        return $this->makeQuantityDivide($work->value, $mass->value * $acceleration->value);
    }

    #[\Override]
    public function createByMile(IMile $mile): ILength
    {
        return $this->makeQuantityProduct($mile->value, self::METRIC_MILE);
    }

    #[\Override]
    public function createByOpticalPower(IOpticalPower $opticalPower): ILength
    {
        return $this->makeQuantityDivide(1, $opticalPower->value);
    }

    #[\Override]
    public function createByParsec(IParsec $parsec): ILength
    {
        return $this->makeQuantityProduct($parsec->value, self::LENGTH_PER_PARSEC);
    }

    #[\Override]
    public function createByTimeVelocity(ITime $time, IVelocity $velocity): ILength
    {
        return $this->makeQuantityProduct($time->value, $velocity->value);
    }

    #[\Override]
    public function createByYard(IYard $yard): ILength
    {
        return $this->makeQuantityProduct($yard->value, self::METRIC_YARD);
    }
}
