<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IMagneticFluxFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFluxDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IMagneticFlux>
 */
final readonly class MagneticFluxFactory extends AMetricFactory implements IMagneticFluxFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IMagneticFlux::class);
    }

    #[\Override]
    public function createByAreaMagneticFluxDensity(
        IArea $area,
        IMagneticFluxDensity $magneticFluxDensity
    ): IMagneticFlux {
        return $this->makeQuantityProduct($magneticFluxDensity->value, $area->value);
    }

    #[\Override]
    public function createByTimeVoltage(ITime $time, IVoltage $voltage): IMagneticFlux
    {
        return $this->makeQuantityProduct($voltage->value, $time->value);
    }
}
