<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use PDobrovolny\Quantity\Contracts\Factory\Metric\ICelsiusFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IThermodynamicTemperature;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ICelsius;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<ICelsius>
 */
final readonly class CelsiusFactory extends AMetricFactory implements ICelsiusFactory
{

    public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ICelsius::class, true);
    }

    #[\Override]
    public function createByThermodynamicTemperature(IThermodynamicTemperature $thermodynamicTemperature): ICelsius
    {
        return $this->create($thermodynamicTemperature->value - self::KELVIN_CELSIUS);
    }
}
