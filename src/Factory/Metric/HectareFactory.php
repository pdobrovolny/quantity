<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IHectareFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IHectare;
use PDobrovolny\Quantity\Enums\EMetricExponent;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IHectare>
 */
final readonly class HectareFactory extends AMetricFactory implements IHectareFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IHectare::class);
    }

    #[\Override]
    public function createByArea(IArea $area): IHectare
    {
        return $this->makeQuantityExp($area->value, mainExponent: EMetricExponent::HEKTO);
    }
}
