<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IVoltAmpereFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IResistance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IVoltAmpere;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IVoltAmpere>
 */
final readonly class VoltAmpereFactory extends AMetricFactory implements IVoltAmpereFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IVoltAmpere::class);
    }

    #[\Override]
    public function createByCurrentResistance(IElectricCurrent $electricCurrent, IResistance $resistance): IVoltAmpere
    {
        return $this->makeQuantityProduct($resistance->value, $electricCurrent->value ** 2);
    }

    #[\Override]
    public function createByCurrentVoltage(IElectricCurrent $electricCurrent, IVoltage $voltage): IVoltAmpere
    {
        return $this->makeQuantityProduct($voltage->value, $electricCurrent->value);
    }

    #[\Override]
    public function createByResistanceVoltage(IResistance $resistance, IVoltage $voltage): IVoltAmpere
    {
        return $this->makeQuantityDivide($voltage->value ** 2, $resistance->value);
    }
}
