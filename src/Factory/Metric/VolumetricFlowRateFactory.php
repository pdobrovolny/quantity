<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IVolumetricFlowRateFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolumetricFlowRate;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IVolumetricFlowRate>
 */
final readonly class VolumetricFlowRateFactory extends AMetricFactory implements IVolumetricFlowRateFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IVolumetricFlowRate::class);
    }

    #[\Override]
    public function createByTimeVolume(ITime $time, IVolume $volume): IVolumetricFlowRate
    {
        return $this->makeQuantityDivide($volume->value, $time->value);
    }
}
