<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IArcMinuteFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcMinute;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcSecond;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDegree;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IArcMinute>
 */
final readonly class ArcMinuteFactory extends AMetricFactory implements IArcMinuteFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IArcMinute::class);
    }

    #[\Override]
    public function arcSecondToArcMinute(IArcSecond $arcSecond): IArcMinute
    {
        return $this->makeQuantityDivide($arcSecond->value, self::DEGREE_SUB_UNIT);
    }

    #[\Override]
    public function degreeToArcMinute(IDegree $degree): IArcMinute
    {
        return $this->makeQuantityProduct($degree->value, self::DEGREE_SUB_UNIT);
    }
}
