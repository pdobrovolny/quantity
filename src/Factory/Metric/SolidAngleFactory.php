<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\ISolidAngleFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILuminousIntensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ILuminousFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ISolidAngle;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<ISolidAngle>
 */
final readonly class SolidAngleFactory extends AMetricFactory implements ISolidAngleFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ISolidAngle::class);
    }

    #[\Override]
    public function createByLuminousIntensityLuminousFlux(
        ILuminousIntensity $luminousIntensity,
        ILuminousFlux $luminousFlux
    ): ISolidAngle {
        return $this->makeQuantityDivide($luminousFlux->value, $luminousIntensity->value);
    }
}
