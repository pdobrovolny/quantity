<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IRadioactivityFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IRadioactivity;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IRadioactivity>
 */
final readonly class RadioactivityFactory extends AMetricFactory implements IRadioactivityFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IRadioactivity::class);
    }

    #[\Override]
    public function timeToRadioactivity(ITime $time): IRadioactivity
    {
        return $this->makeQuantityDivide(1, $time->value);
    }
}
