<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IDensityFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IDensity>
 */
final readonly class DensityFactory extends AMetricFactory implements IDensityFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IDensity::class);
    }

    #[\Override]
    public function createByLengthAccelerationAreaForce(
        ILength $length,
        IAcceleration $acceleration,
        IArea $area,
        IForce $force
    ): IDensity {
        return $this->makeQuantityDivide($force->value, $area->value * $length->value * $acceleration->value);
    }

    #[\Override]
    public function createByLengthAccelerationPressure(
        ILength $length,
        IAcceleration $acceleration,
        IPressure $pressure
    ): IDensity {
        return $this->makeQuantityDivide($pressure->value, $length->value * $acceleration->value);
    }

    #[\Override]
    public function createByMassVolume(IMass $mass, IVolume $volume): IDensity
    {
        return $this->makeQuantityDivide($mass->value, $volume->value);
    }
}
