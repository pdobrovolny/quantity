<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IAreaFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IIlluminance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ILuminousFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFluxDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IHectare;
use PDobrovolny\Quantity\Enums\EMetricExponent;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IArea>
 */
final readonly class AreaFactory extends AMetricFactory implements IAreaFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IArea::class);
    }

    #[\Override]
    public function createByForcePressure(IForce $force, IPressure $pressure): IArea
    {
        return $this->makeQuantityDivide($force->value, $pressure->value);
    }

    #[\Override]
    public function createByForceWork(IForce $force, IWork $work): IArea
    {
        return $this->makeQuantityDivide($force->value, $work->value);
    }

    #[\Override]
    public function createByHectare(IHectare $hectare): IArea
    {
        return $this->makeQuantityExp($hectare->value, mainExponent: EMetricExponent::DECI);
    }

    #[\Override]
    public function createByIlluminanceLuminousFlux(IIlluminance $illuminance, ILuminousFlux $luminousFlux): IArea
    {
        return $this->makeQuantityDivide($luminousFlux->value, $illuminance->value);
    }

    #[\Override]
    public function createByLengthAccelerationForceDensity(
        ILength $length,
        IAcceleration $acceleration,
        IForce $force,
        IDensity $density
    ): IArea {
        return $this->makeQuantityDivide(
            $force->value,
            $length->value * $density->value * $acceleration->value
        );
    }

    #[\Override]
    public function createByLengthPressureWork(ILength $length, IPressure $pressure, IWork $work): IArea
    {
        return $this->makeQuantityDivide($work->value, $pressure->value * $length->value);
    }

    #[\Override]
    public function createByMagneticFluxMagneticFluxDensity(
        IMagneticFlux $magneticFlux,
        IMagneticFluxDensity $magneticFluxDensity
    ): IArea {
        return $this->makeQuantityDivide($magneticFlux->value, $magneticFluxDensity->value);
    }
}
