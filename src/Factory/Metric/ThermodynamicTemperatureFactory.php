<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IThermodynamicTemperatureFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFahrenheit;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IThermodynamicTemperature;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ICelsius;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IThermodynamicTemperature>
 */
final readonly class ThermodynamicTemperatureFactory extends AMetricFactory implements IThermodynamicTemperatureFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IThermodynamicTemperature::class);
    }

    #[\Override]
    public function celsiusToThermodynamicTemperature(ICelsius $celsius): IThermodynamicTemperature
    {
        return $this->create($celsius->value + self::KELVIN_CELSIUS);
    }

    #[\Override]
    public function fahrenheitToThermodynamicTemperature(IFahrenheit $fahrenheit): IThermodynamicTemperature
    {
        return $this->create(($fahrenheit->value - 32) / 1.8 + self::KELVIN_CELSIUS);
    }
}
