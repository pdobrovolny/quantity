<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\ITonneFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ITonne;
use PDobrovolny\Quantity\Enums\EMetricExponent;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<ITonne>
 */
final readonly class TonneFactory extends AMetricFactory implements ITonneFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ITonne::class);
    }

    #[\Override]
    public function createByMass(IMass $mass): ITonne
    {
        return $this->makeQuantityExp($mass->value, EMetricExponent::MILI);
    }
}
