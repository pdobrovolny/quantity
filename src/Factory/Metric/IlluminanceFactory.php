<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IIlluminanceFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IIlluminance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ILuminousFlux;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IIlluminance>
 */
final readonly class IlluminanceFactory extends AMetricFactory implements IIlluminanceFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IIlluminance::class);
    }

    #[\Override]
    public function createByAreaLuminousFlux(IArea $area, ILuminousFlux $luminousFlux): IIlluminance
    {
        return $this->makeQuantityDivide($luminousFlux->value, $area->value);
    }
}
