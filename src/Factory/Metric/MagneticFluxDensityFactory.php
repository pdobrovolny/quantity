<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IMagneticFluxDensityFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFluxDensity;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IMagneticFluxDensity>
 */
final readonly class MagneticFluxDensityFactory extends AMetricFactory implements IMagneticFluxDensityFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IMagneticFluxDensity::class);
    }

    #[\Override]
    public function createByAreaMagneticFlux(IArea $area, IMagneticFlux $magneticFlux): IMagneticFluxDensity
    {
        return $this->makeQuantityDivide($magneticFlux->value, $area->value);
    }
}
