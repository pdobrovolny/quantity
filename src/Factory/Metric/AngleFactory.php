<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IAngleFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngle;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngularVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDegree;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IAngle>
 */
final readonly class AngleFactory extends AMetricFactory implements IAngleFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IAngle::class);
    }

    #[\Override]
    public function createByDegree(IDegree $degree): IAngle
    {
        return $this->create(self::ANGLE_MAX / self::DEGREE_MAX * $degree->value);
    }

    #[\Override]
    public function createByTimeAngularVelocity(ITime $time, IAngularVelocity $angularVelocity): IAngle
    {
        return $this->makeQuantityProduct($angularVelocity->value, $time->value);
    }
}
