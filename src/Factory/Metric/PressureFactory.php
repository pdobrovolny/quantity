<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IPressureFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IPressure>
 */
final readonly class PressureFactory extends AMetricFactory implements IPressureFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IPressure::class);
    }

    #[\Override]
    public function createByAreaForce(IArea $area, IForce $force): IPressure
    {
        return $this->makeQuantityDivide($force->value, $area->value);
    }

    #[\Override]
    public function createByLengthAccelerationDensity(
        ILength $length,
        IAcceleration $acceleration,
        IDensity $density
    ): IPressure {
        return $this->makeQuantityProduct($length->value, $density->value, $acceleration->value);
    }

    #[\Override]
    public function createByLengthAreaWork(ILength $length, IArea $area, IWork $work): IPressure
    {
        return $this->makeQuantityDivide($work->value, $area->value * $length->value);
    }

    #[\Override]
    public function createByVolumeWork(IVolume $volume, IWork $work): IPressure
    {
        return $this->makeQuantityDivide($work->value, $volume->value);
    }
}
