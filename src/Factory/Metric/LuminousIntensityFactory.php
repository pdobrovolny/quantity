<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\ILuminousIntensityFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILuminousIntensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ILuminousFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ISolidAngle;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<ILuminousIntensity>
 */
final readonly class LuminousIntensityFactory extends AMetricFactory implements ILuminousIntensityFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ILuminousIntensity::class);
    }

    #[\Override]
    public function luminousFluxSolidAngleToLuminousIntensity(
        ILuminousFlux $luminousFlux,
        ISolidAngle $solidAngle
    ): ILuminousIntensity {
        return $this->makeQuantityDivide($luminousFlux->value, $solidAngle->value);
    }
}
