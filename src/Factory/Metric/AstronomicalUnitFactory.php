<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IAstronomicalUnitFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IAstronomicalUnit;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IAstronomicalUnit>
 */
final readonly class AstronomicalUnitFactory extends AMetricFactory implements IAstronomicalUnitFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IAstronomicalUnit::class);
    }

    #[\Override]
    public function createByLength(ILength $length): IAstronomicalUnit
    {
        return $this->makeQuantityDivide($length->value, self::LENGTH_PER_ASTRO);
    }
}
