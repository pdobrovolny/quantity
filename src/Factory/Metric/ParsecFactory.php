<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IParsecFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IParsec;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IParsec>
 */
final readonly class ParsecFactory extends AMetricFactory implements IParsecFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IParsec::class);
    }

    #[\Override]
    public function createByLength(ILength $length): IParsec
    {
        return $this->makeQuantityDivide($length->value, self::LENGTH_PER_PARSEC);
    }
}
