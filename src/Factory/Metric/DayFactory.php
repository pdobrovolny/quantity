<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IDayFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDay;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IDay>
 */
final readonly class DayFactory extends AMetricFactory implements IDayFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IDay::class);
    }

    #[\Override]
    public function createByTime(ITime $time): IDay
    {
        return $this->makeQuantityDivide($time->value, self::SEC_PER_DAY);
    }
}
