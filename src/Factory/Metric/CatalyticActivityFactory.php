<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\ICatalyticActivityFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IAmountOfSubstance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICatalyticActivity;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<ICatalyticActivity>
 */
final readonly class CatalyticActivityFactory extends AMetricFactory implements ICatalyticActivityFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ICatalyticActivity::class);
    }

    #[\Override]
    public function createByAmountOfSubstanceTime(
        IAmountOfSubstance $amountOfSubstance,
        ITime $time
    ): ICatalyticActivity {
        return $this->makeQuantityDivide($amountOfSubstance->value, $time->value);
    }
}
