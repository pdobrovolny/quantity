<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IOpticalPowerFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IOpticalPower;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IOpticalPower>
 */
final readonly class OpticalPowerFactory extends AMetricFactory implements IOpticalPowerFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IOpticalPower::class);
    }

    #[\Override]
    public function createByLength(ILength $length): IOpticalPower
    {
        return $this->makeQuantityDivide(1, $length->value);
    }
}
