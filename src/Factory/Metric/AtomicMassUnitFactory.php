<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IAtomicMassUnitFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IAtomicMassUnit;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<IAtomicMassUnit>
 */
final readonly class AtomicMassUnitFactory extends AMetricFactory implements IAtomicMassUnitFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, IAtomicMassUnit::class);
    }

    #[\Override]
    public function massToAtomicMassUnit(IMass $mass): IAtomicMassUnit
    {
        return $this->makeQuantityDivide($mass->value, self::MASS_PER_ATOMIC_MASS);
    }
}
