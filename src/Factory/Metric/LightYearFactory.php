<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory\Metric;

use DI\FactoryInterface;
use JetBrains\PhpStorm\Pure;
use PDobrovolny\Quantity\Contracts\Factory\Metric\ILightYearFactory;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ILightYear;
use PDobrovolny\Quantity\Factory\AMetricFactory;

/**
 * @template-extends AMetricFactory<ILightYear>
 */
final readonly class LightYearFactory extends AMetricFactory implements ILightYearFactory
{
    #[Pure] public function __construct(FactoryInterface $factory)
    {
        parent::__construct($factory, ILightYear::class);
    }

    #[\Override]
    public function createByLength(ILength $length): ILightYear
    {
        return $this->makeQuantityDivide($length->value, self::LENGTH_PER_LIGHT_YEAR);
    }
}
