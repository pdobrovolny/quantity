<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory;

use DI\FactoryInterface;
use PDobrovolny\Quantity\Contracts\Factory\Imperial\IQuantityImperialFactory;
use PDobrovolny\Quantity\Contracts\Quantity\IImperial;
use PDobrovolny\Quantity\Enums\EDimensions;

/**
 * @template TQuantity of IImperial
 *
 * @template-implements IQuantityImperialFactory<TQuantity>
 */
abstract readonly class AImperialFactory implements IQuantityImperialFactory
{
    /** @var class-string<TQuantity> */
    public string $class;
    public EDimensions $dimensions;

    /**
     * @param class-string<TQuantity> $interface
     */
    public function __construct(
        FactoryInterface $factory,
        public string $interface,
        private bool $allowNegative = false
    ) {
        $this->class = $factory->make($this->interface, ['value' => 0])::class;
        $this->dimensions = EDimensions::fromQuantity($this->interface);
    }

    #[\Override]
    final public function create(float $value): IImperial
    {
        \assert($value >= 0. || \is_nan($value) || $this->allowNegative);

        return new $this->class($value);
    }

}
