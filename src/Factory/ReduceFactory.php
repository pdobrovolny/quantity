<?php

declare(strict_types=1);

namespace PDobrovolny\Quantity\Factory;

use PDobrovolny\Quantity\Contracts\Factory\Metric\IAngleFactory;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngle;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcMinute;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcSecond;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDegree;

final readonly class ReduceFactory
{
    /**
     * @template T of IQuantity
     *
     * @param T $quantity
     *
     * @return T
     */
    public function reduce(IQuantity $quantity): IQuantity
    {
        return new $quantity(
            self::reduceValue(
                $quantity->value,
                match (true) {
                    $quantity instanceof IAngle => IAngleFactory::ANGLE_MAX,
                    $quantity instanceof IArcMinute, $quantity instanceof IArcSecond => IAngleFactory::DEGREE_SUB_UNIT,
                    $quantity instanceof IDegree => IAngleFactory::DEGREE_MAX,
                    default => $quantity->value
                }
            )
        );
    }

    private static function reduceValue(float $value, float $maxValue): float
    {
        return $value - \floor($value / $maxValue) * $maxValue;
    }
}
