<?php

declare(strict_types=1);

use PDobrovolny\Quantity\Contracts\Factory\Imperial\IQuantityImperialFactory;
use PDobrovolny\Quantity\Contracts\Factory\Metric\IQuantityMetricFactory;
use PDobrovolny\Quantity\Contracts\Quantity\IImperial;
use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;

arch()->preset()->php();
arch()->preset()->security()->ignoring('assert');

arch('globals')
    ->expect('PDobrovolny\Quantity')
    ->toUseStrictTypes()
    ->toUseStrictEquality()
    ->not->toHaveProtectedMethods()
    ->not->toBeTrait();

arch('contracts')
    ->expect('PDobrovolny\Quantity\Factory\Contracts')
    ->toBeInterfaces();

arch('enums')
    ->expect('PDobrovolny\Quantity\Enums')
    ->toBeIntBackedEnums();

arch('containers')
    ->expect('PDobrovolny\Quantity\Container')
    ->toBeClass()
    ->toBeReadonly()
    ->classes()->toExtend(IQuantity::class);

arch('containers → metric')
    ->expect('PDobrovolny\Quantity\Container\Metric')
    ->toBeFinal()
    ->classes()->toExtend(IMetric::class);

arch('containers → imperial')
    ->expect('PDobrovolny\Quantity\Container\Imperial')
    ->toBeFinal()
    ->classes()->toExtend(IImperial::class);

arch('factories')
    ->expect('PDobrovolny\Quantity\Factory')
    ->toBeReadonly()
    ->toHaveSuffix('Factory');

arch('factories → metric')
    ->expect('PDobrovolny\Quantity\Factory\Metric')
    ->toBeFinal()
    ->classes()->toExtend(IQuantityMetricFactory::class);

arch('factories → imperial')
    ->expect('PDobrovolny\Quantity\Factory\Imperial')
    ->toBeFinal()
    ->classes()->toExtend(IQuantityImperialFactory::class);

arch('formatters')
    ->expect('PDobrovolny\Quantity\Formatter')
    ->toBeReadonly()
    ->toHaveSuffix('Formatter')
    ->toHaveMethod('format')
    ->toBeFinal();

