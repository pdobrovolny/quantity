<?php

declare(strict_types=1);

use PDobrovolny\Quantity\Container\Imperial\Basic\Chain;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicChain;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicFoot;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicFurlong;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicInch;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicLeague;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicMile;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicYard;
use PDobrovolny\Quantity\Container\Imperial\Basic\Fahrenheit;
use PDobrovolny\Quantity\Container\Imperial\Basic\Foot;
use PDobrovolny\Quantity\Container\Imperial\Basic\Furlong;
use PDobrovolny\Quantity\Container\Imperial\Basic\Grain;
use PDobrovolny\Quantity\Container\Imperial\Basic\Hundredweight;
use PDobrovolny\Quantity\Container\Imperial\Basic\Inch;
use PDobrovolny\Quantity\Container\Imperial\Basic\League;
use PDobrovolny\Quantity\Container\Imperial\Basic\Mile;
use PDobrovolny\Quantity\Container\Imperial\Basic\Pound;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareChain;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareFoot;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareFurlong;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareInch;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareLeague;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareMile;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareYard;
use PDobrovolny\Quantity\Container\Imperial\Basic\Stone;
use PDobrovolny\Quantity\Container\Imperial\Basic\Ton;
use PDobrovolny\Quantity\Container\Imperial\Basic\Yard;
use PDobrovolny\Quantity\Container\Imperial\US\FluidOunce;
use PDobrovolny\Quantity\Container\Imperial\US\Gallon;
use PDobrovolny\Quantity\Container\Imperial\US\Gill;
use PDobrovolny\Quantity\Container\Imperial\US\Pint;
use PDobrovolny\Quantity\Container\Imperial\US\Quart;
use PDobrovolny\Quantity\Container\Metric\Basic\AmountOfSubstance;
use PDobrovolny\Quantity\Container\Metric\Basic\ElectricCurrent;
use PDobrovolny\Quantity\Container\Metric\Basic\Length;
use PDobrovolny\Quantity\Container\Metric\Basic\LuminousIntensity;
use PDobrovolny\Quantity\Container\Metric\Basic\Mass;
use PDobrovolny\Quantity\Container\Metric\Basic\ThermodynamicTemperature;
use PDobrovolny\Quantity\Container\Metric\Basic\Time;
use PDobrovolny\Quantity\Container\Metric\Derived\AbsorbedDose;
use PDobrovolny\Quantity\Container\Metric\Derived\Acceleration;
use PDobrovolny\Quantity\Container\Metric\Derived\Angle;
use PDobrovolny\Quantity\Container\Metric\Derived\AngularVelocity;
use PDobrovolny\Quantity\Container\Metric\Derived\Area;
use PDobrovolny\Quantity\Container\Metric\Derived\Capacitance;
use PDobrovolny\Quantity\Container\Metric\Derived\CatalyticActivity;
use PDobrovolny\Quantity\Container\Metric\Derived\Charge;
use PDobrovolny\Quantity\Container\Metric\Derived\Conductance;
use PDobrovolny\Quantity\Container\Metric\Derived\Density;
use PDobrovolny\Quantity\Container\Metric\Derived\EquivalentDose;
use PDobrovolny\Quantity\Container\Metric\Derived\Force;
use PDobrovolny\Quantity\Container\Metric\Derived\Frequency;
use PDobrovolny\Quantity\Container\Metric\Derived\Illuminance;
use PDobrovolny\Quantity\Container\Metric\Derived\Inductance;
use PDobrovolny\Quantity\Container\Metric\Derived\LuminousFlux;
use PDobrovolny\Quantity\Container\Metric\Derived\MagneticFlux;
use PDobrovolny\Quantity\Container\Metric\Derived\MagneticFluxDensity;
use PDobrovolny\Quantity\Container\Metric\Derived\Power;
use PDobrovolny\Quantity\Container\Metric\Derived\Pressure;
use PDobrovolny\Quantity\Container\Metric\Derived\Radioactivity;
use PDobrovolny\Quantity\Container\Metric\Derived\Resistance;
use PDobrovolny\Quantity\Container\Metric\Derived\SolidAngle;
use PDobrovolny\Quantity\Container\Metric\Derived\Velocity;
use PDobrovolny\Quantity\Container\Metric\Derived\Voltage;
use PDobrovolny\Quantity\Container\Metric\Derived\Volume;
use PDobrovolny\Quantity\Container\Metric\Derived\VolumetricFlowRate;
use PDobrovolny\Quantity\Container\Metric\Derived\Work;
use PDobrovolny\Quantity\Container\Metric\Sub\ArcMinute;
use PDobrovolny\Quantity\Container\Metric\Sub\ArcSecond;
use PDobrovolny\Quantity\Container\Metric\Sub\AstronomicalUnit;
use PDobrovolny\Quantity\Container\Metric\Sub\AtomicMassUnit;
use PDobrovolny\Quantity\Container\Metric\Sub\Celsius;
use PDobrovolny\Quantity\Container\Metric\Sub\Day;
use PDobrovolny\Quantity\Container\Metric\Sub\Degree;
use PDobrovolny\Quantity\Container\Metric\Sub\Hectare;
use PDobrovolny\Quantity\Container\Metric\Sub\Hour;
use PDobrovolny\Quantity\Container\Metric\Sub\LightYear;
use PDobrovolny\Quantity\Container\Metric\Sub\Liter;
use PDobrovolny\Quantity\Container\Metric\Sub\Minute;
use PDobrovolny\Quantity\Container\Metric\Sub\OpticalPower;
use PDobrovolny\Quantity\Container\Metric\Sub\Parsec;
use PDobrovolny\Quantity\Container\Metric\Sub\Tonne;
use PDobrovolny\Quantity\Container\Metric\Sub\VoltAmpere;
use PDobrovolny\Quantity\Enums\EMetricExponent;
use PDobrovolny\Quantity\Formatter\QuantityFormatter;

require_once __DIR__ . '/../../TestHelper.php';

it('formatDefault', function () {
    Locale::setDefault('en_US');

    expect(new QuantityFormatter()->format(new Length(M_PI * 1000)))
        ->toBe('3,141.593 m');
});

it(
    'format',
    function (string $expected, string $class) {
        expect($this->formatter->format(new $class(1.), EMetricExponent::DECI))
            ->toBe($expected, $class);
    }
)->with(static function () {
    $result = [];
    foreach (getAllQuantities() as $quantity) {
        $result[] = [
            match ($quantity) {
                \PDobrovolny\Quantity\Container\Imperial\UK\Gallon::class => '1 UK fl·gal',
                \PDobrovolny\Quantity\Container\Imperial\UK\Quart::class => '1 UK fl·qt',
                \PDobrovolny\Quantity\Container\Imperial\UK\Pint::class => '1 UK fl·pt',
                \PDobrovolny\Quantity\Container\Imperial\UK\FluidOunce::class => '1 UK fl·oz',
                \PDobrovolny\Quantity\Container\Imperial\UK\Gill::class => '1 UK fl·gi',
                SquareInch::class => '1 in²',
                Hundredweight::class => '1 cwt',
                CubicFurlong::class => '1 fur³',
                CubicLeague::class => '1 lea³',
                Ton::class => '1 ton',
                Yard::class => '1 yd',
                Chain::class => '1 ch',
                SquareFurlong::class => '1 fur²',
                SquareLeague::class => '1 lea²',
                CubicMile::class => '1 mi³',
                CubicFoot::class => '1 ft³',
                SquareMile::class => '1 mi²',
                Mile::class => '1 mi',
                Grain::class => '1 gr',
                SquareFoot::class => '1 ft²',
                Furlong::class => '1 fur',
                Stone::class => '1 st',
                CubicChain::class => '1 ch³',
                CubicInch::class => '1 in³',
                SquareChain::class => '1 ch²',
                Fahrenheit::class => '1 °F',
                Foot::class => '1 ft',
                CubicYard::class => '1 yd³',
                Inch::class => '1 in',
                SquareYard::class => '1 yd²',
                Pound::class => '1 lb',
                League::class => '1 lea',
                Quart::class => '1 US liq·qt',
                FluidOunce::class => '1 US fl·oz',
                Gallon::class => '1 US liq·gal',
                Pint::class => '1 US liq·pt',
                Gill::class => '1 US liq·gi',
                CatalyticActivity::class => '10 dkat',
                AbsorbedDose::class => '10 dGy',
                LuminousFlux::class => '10 dlm',
                Area::class => '100 dm²',
                Acceleration::class => '10 dm/s²',
                Conductance::class => '10 dS',
                Angle::class => '10 drad',
                MagneticFlux::class => '10 dWb',
                MagneticFluxDensity::class => '10 dT',
                Charge::class => '10 dC',
                Voltage::class => '10 dV',
                EquivalentDose::class => '10 dSv',
                Resistance::class => '10 dΩ',
                Volume::class => '1000 dm³',
                SolidAngle::class => '10 dsr',
                Work::class => '10 dJ',
                Power::class => '10 dW',
                Pressure::class => '10 dPa',
                Inductance::class => '10 dH',
                Radioactivity::class => '10 dBq',
                Frequency::class => '10 dHz',
                Illuminance::class => '10 dlx',
                VolumetricFlowRate::class => '1000 dm³/s',
                AngularVelocity::class => '10 drad/s',
                Capacitance::class => '10 dF',
                Velocity::class => '10 dm/s',
                Force::class => '10 dN',
                Density::class => '100 dg/m³',
                Celsius::class => '10 d°C',
                VoltAmpere::class => '10 dVA',
                AtomicMassUnit::class => '10 du',
                AstronomicalUnit::class => '10 dAU',
                Hectare::class => '10 dha',
                ArcMinute::class => '10 d\'',
                ArcSecond::class => '10 d"',
                Liter::class => '10 dl',
                OpticalPower::class => '10 dD',
                LightYear::class => '10 dly',
                Parsec::class => '10 dpc',
                Minute::class => '10 dmin',
                Hour::class => '10 dh',
                Degree::class => '10 d°',
                Day::class => '10 dd',
                Tonne::class => '10 dt',
                Time::class => '10 ds',
                ElectricCurrent::class => '10 dA',
                ThermodynamicTemperature::class => '10 dK',
                LuminousIntensity::class => '10 dcd',
                Mass::class => '100 dg',
                Length::class => '10 dm',
                AmountOfSubstance::class => '10 dmol',
                default => 'todo: ' . $quantity
            },
            $quantity,
        ];
    }

    return $result;
});

beforeEach(
    function () {
        $this->formatter = new QuantityFormatter(new NumberFormatter('cs_CZ', NumberFormatter::PATTERN_DECIMAL));
    }
);
