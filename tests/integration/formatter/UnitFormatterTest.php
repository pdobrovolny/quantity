<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use PDobrovolny\Quantity\Container\Imperial\Basic\Chain;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicChain;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicFoot;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicFurlong;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicInch;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicLeague;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicMile;
use PDobrovolny\Quantity\Container\Imperial\Basic\CubicYard;
use PDobrovolny\Quantity\Container\Imperial\Basic\Fahrenheit;
use PDobrovolny\Quantity\Container\Imperial\Basic\Foot;
use PDobrovolny\Quantity\Container\Imperial\Basic\Furlong;
use PDobrovolny\Quantity\Container\Imperial\Basic\Grain;
use PDobrovolny\Quantity\Container\Imperial\Basic\Hundredweight;
use PDobrovolny\Quantity\Container\Imperial\Basic\Inch;
use PDobrovolny\Quantity\Container\Imperial\Basic\League;
use PDobrovolny\Quantity\Container\Imperial\Basic\Mile;
use PDobrovolny\Quantity\Container\Imperial\Basic\Pound;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareChain;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareFoot;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareFurlong;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareInch;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareLeague;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareMile;
use PDobrovolny\Quantity\Container\Imperial\Basic\SquareYard;
use PDobrovolny\Quantity\Container\Imperial\Basic\Stone;
use PDobrovolny\Quantity\Container\Imperial\Basic\Ton;
use PDobrovolny\Quantity\Container\Imperial\Basic\Yard;
use PDobrovolny\Quantity\Container\Imperial\US\FluidOunce;
use PDobrovolny\Quantity\Container\Imperial\US\Gallon;
use PDobrovolny\Quantity\Container\Imperial\US\Gill;
use PDobrovolny\Quantity\Container\Imperial\US\Pint;
use PDobrovolny\Quantity\Container\Imperial\US\Quart;
use PDobrovolny\Quantity\Container\Metric\Basic\AmountOfSubstance;
use PDobrovolny\Quantity\Container\Metric\Basic\ElectricCurrent;
use PDobrovolny\Quantity\Container\Metric\Basic\Length;
use PDobrovolny\Quantity\Container\Metric\Basic\LuminousIntensity;
use PDobrovolny\Quantity\Container\Metric\Basic\Mass;
use PDobrovolny\Quantity\Container\Metric\Basic\ThermodynamicTemperature;
use PDobrovolny\Quantity\Container\Metric\Basic\Time;
use PDobrovolny\Quantity\Container\Metric\Derived\AbsorbedDose;
use PDobrovolny\Quantity\Container\Metric\Derived\Acceleration;
use PDobrovolny\Quantity\Container\Metric\Derived\Angle;
use PDobrovolny\Quantity\Container\Metric\Derived\AngularVelocity;
use PDobrovolny\Quantity\Container\Metric\Derived\Area;
use PDobrovolny\Quantity\Container\Metric\Derived\Capacitance;
use PDobrovolny\Quantity\Container\Metric\Derived\CatalyticActivity;
use PDobrovolny\Quantity\Container\Metric\Derived\Charge;
use PDobrovolny\Quantity\Container\Metric\Derived\Conductance;
use PDobrovolny\Quantity\Container\Metric\Derived\Density;
use PDobrovolny\Quantity\Container\Metric\Derived\EquivalentDose;
use PDobrovolny\Quantity\Container\Metric\Derived\Force;
use PDobrovolny\Quantity\Container\Metric\Derived\Frequency;
use PDobrovolny\Quantity\Container\Metric\Derived\Illuminance;
use PDobrovolny\Quantity\Container\Metric\Derived\Inductance;
use PDobrovolny\Quantity\Container\Metric\Derived\LuminousFlux;
use PDobrovolny\Quantity\Container\Metric\Derived\MagneticFlux;
use PDobrovolny\Quantity\Container\Metric\Derived\MagneticFluxDensity;
use PDobrovolny\Quantity\Container\Metric\Derived\Power;
use PDobrovolny\Quantity\Container\Metric\Derived\Pressure;
use PDobrovolny\Quantity\Container\Metric\Derived\Radioactivity;
use PDobrovolny\Quantity\Container\Metric\Derived\Resistance;
use PDobrovolny\Quantity\Container\Metric\Derived\SolidAngle;
use PDobrovolny\Quantity\Container\Metric\Derived\Velocity;
use PDobrovolny\Quantity\Container\Metric\Derived\Voltage;
use PDobrovolny\Quantity\Container\Metric\Derived\Volume;
use PDobrovolny\Quantity\Container\Metric\Derived\VolumetricFlowRate;
use PDobrovolny\Quantity\Container\Metric\Derived\Work;
use PDobrovolny\Quantity\Container\Metric\Sub\ArcMinute;
use PDobrovolny\Quantity\Container\Metric\Sub\ArcSecond;
use PDobrovolny\Quantity\Container\Metric\Sub\AstronomicalUnit;
use PDobrovolny\Quantity\Container\Metric\Sub\AtomicMassUnit;
use PDobrovolny\Quantity\Container\Metric\Sub\Celsius;
use PDobrovolny\Quantity\Container\Metric\Sub\Day;
use PDobrovolny\Quantity\Container\Metric\Sub\Degree;
use PDobrovolny\Quantity\Container\Metric\Sub\Hectare;
use PDobrovolny\Quantity\Container\Metric\Sub\Hour;
use PDobrovolny\Quantity\Container\Metric\Sub\LightYear;
use PDobrovolny\Quantity\Container\Metric\Sub\Liter;
use PDobrovolny\Quantity\Container\Metric\Sub\Minute;
use PDobrovolny\Quantity\Container\Metric\Sub\OpticalPower;
use PDobrovolny\Quantity\Container\Metric\Sub\Parsec;
use PDobrovolny\Quantity\Container\Metric\Sub\Tonne;
use PDobrovolny\Quantity\Container\Metric\Sub\VoltAmpere;
use PDobrovolny\Quantity\Formatter\UnitFormatter;

require_once __DIR__ . '/../../TestHelper.php';

it(
    'format',
    function (string $expected, string $class) {
        expect($this->unitFormatter->format(new $class(1.)))
            ->toBe($expected, $class);
    }
)->with(static function () {
    $result = [];
    foreach (getAllQuantities() as $quantity) {
        $result[] = [
            match ($quantity) {
                \PDobrovolny\Quantity\Container\Imperial\UK\Gallon::class => 'UK fl·gal',
                \PDobrovolny\Quantity\Container\Imperial\UK\Quart::class => 'UK fl·qt',
                \PDobrovolny\Quantity\Container\Imperial\UK\Pint::class => 'UK fl·pt',
                \PDobrovolny\Quantity\Container\Imperial\UK\FluidOunce::class => 'UK fl·oz',
                \PDobrovolny\Quantity\Container\Imperial\UK\Gill::class => 'UK fl·gi',
                SquareInch::class => 'in²',
                Hundredweight::class => 'cwt',
                CubicFurlong::class => 'fur³',
                CubicLeague::class => 'lea³',
                Ton::class => 'ton',
                Yard::class => 'yd',
                Chain::class => 'ch',
                SquareFurlong::class => 'fur²',
                SquareLeague::class => 'lea²',
                CubicMile::class => 'mi³',
                CubicFoot::class => 'ft³',
                SquareMile::class => 'mi²',
                Mile::class => 'mi',
                Grain::class => 'gr',
                SquareFoot::class => 'ft²',
                Furlong::class => 'fur',
                Stone::class => 'st',
                CubicChain::class => 'ch³',
                CubicInch::class => 'in³',
                SquareChain::class => 'ch²',
                Fahrenheit::class => '°F',
                Foot::class => 'ft',
                CubicYard::class => 'yd³',
                Inch::class => 'in',
                SquareYard::class => 'yd²',
                Pound::class => 'lb',
                League::class => 'lea',
                Quart::class => 'US liq·qt',
                FluidOunce::class => 'US fl·oz',
                Gallon::class => 'US liq·gal',
                Pint::class => 'US liq·pt',
                Gill::class => 'US liq·gi',
                CatalyticActivity::class => 'kat',
                AbsorbedDose::class => 'Gy',
                LuminousFlux::class => 'lm',
                Area::class => 'm²',
                Acceleration::class => 'm/s²',
                Conductance::class => 'S',
                Angle::class => 'rad',
                MagneticFlux::class => 'Wb',
                MagneticFluxDensity::class => 'T',
                Charge::class => 'C',
                Voltage::class => 'V',
                EquivalentDose::class => 'Sv',
                Resistance::class => 'Ω',
                Volume::class => 'm³',
                SolidAngle::class => 'sr',
                Work::class => 'J',
                Power::class => 'W',
                Pressure::class => 'Pa',
                Inductance::class => 'H',
                Radioactivity::class => 'Bq',
                Frequency::class => 'Hz',
                Illuminance::class => 'lx',
                VolumetricFlowRate::class => 'm³/s',
                AngularVelocity::class => 'rad/s',
                Capacitance::class => 'F',
                Velocity::class => 'm/s',
                Force::class => 'N',
                Density::class => 'kg/m³',
                Celsius::class => '°C',
                VoltAmpere::class => 'VA',
                AtomicMassUnit::class => 'u',
                AstronomicalUnit::class => 'AU',
                Hectare::class => 'ha',
                ArcMinute::class => '\'',
                ArcSecond::class => '"',
                Liter::class => 'l',
                OpticalPower::class => 'D',
                LightYear::class => 'ly',
                Parsec::class => 'pc',
                Minute::class => 'min',
                Hour::class => 'h',
                Degree::class => '°',
                Day::class => 'd',
                Tonne::class => 't',
                Time::class => 's',
                ElectricCurrent::class => 'A',
                ThermodynamicTemperature::class => 'K',
                LuminousIntensity::class => 'cd',
                Mass::class => 'kg',
                Length::class => 'm',
                AmountOfSubstance::class => 'mol',
                default => 'todo: ' . $quantity
            },
            $quantity,
        ];
    }

    return $result;
}
);

beforeEach(
    function () {
        $this->unitFormatter = new ContainerBuilder()->build()
            ->get(UnitFormatter::class);
    }
);
