<?php

declare(strict_types=1);

use PDobrovolny\Quantity\Factory\DensityFactory;
use PDobrovolny\Quantity\Factory\QuantityFactory;

it(
    'main',
    function (float $expectedValue, string $compound) {
        expect(new DensityFactory(new QuantityFactory())->{$compound}()->value)
            ->toBe($expectedValue);
    }
)->with(
    [
        [2700., 'aluminium'],
        [725., 'benzine'],
        [2250., 'carbon'],
        [678., 'cng'],
        [8960., 'copper'],
        [840., 'diesel'],
        [2800., 'dural'],
        [789.3, 'ethanol'],
        [2600., 'glass'],
        [19320., 'gold'],
        [985., 'humanBodyAvg'],
        [0.08895, 'hydrogen'],
        [42., 'hydrogen700bar'],
        [71., 'hydrogenLiquid'],
        [916.8, 'ice'],
        [7870., 'iron'],
        [11340., 'lead'],
        [540.5, 'lpg'],
        [13579.04, 'mercury'],
        [791.7, 'methanol'],
        [900., 'paper'],
        [865., 'petroleum'],
        [21450., 'platina'],
        [2160., 'salt'],
        [1024., 'seaWater'],
        [10500., 'silver'],
        [7850., 'steel'],
        [4540., 'titan'],
        [998., 'water'],
        [789.9, 'acetone'],
    ]
);
