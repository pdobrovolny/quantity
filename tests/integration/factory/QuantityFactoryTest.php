<?php

declare(strict_types=1);

use PDobrovolny\Quantity\Container\Metric\Basic\Length;
use PDobrovolny\Quantity\Container\Metric\Basic\Time;
use PDobrovolny\Quantity\Container\Metric\Derived\Volume;
use PDobrovolny\Quantity\Contracts\Quantity\IImperial;
use PDobrovolny\Quantity\Contracts\Quantity\IMetric;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IChain;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFahrenheit;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFoot;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IFurlong;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IGrain;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IHundredweight;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IInch;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ILeague;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IMile;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IPound;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IStone;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\ITon;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\Basic\IYard;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\IImperialUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\IImperialUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IFluidOunce as IFluidOunceUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGallon as IGallonUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IGill as IGillUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IPint as IPintUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\UK\IQuart as IQuartUK;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IFluidOunce as IFluidOunceUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGallon as IGallonUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IGill as IGillUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IPint as IPintUS;
use PDobrovolny\Quantity\Contracts\Quantity\Imperial\US\IQuart as IQuartUS;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity2D;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity3D;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IAmountOfSubstance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IElectricCurrent;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILength;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ILuminousIntensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IMass;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\IThermodynamicTemperature;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Basic\ITime;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAbsorbedDose;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAcceleration;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngle;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IAngularVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IArea;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICapacitance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICatalyticActivity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ICharge;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IConductance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IEquivalentDose;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IForce;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IFrequency;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IIlluminance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ILuminousFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFlux;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IMagneticFluxDensity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IPressure;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IRadioactivity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IResistance;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\ISolidAngle;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVelocity;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVoltage;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolume;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IVolumetricFlowRate;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Derived\IWork;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcMinute;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IArcSecond;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IAstronomicalUnit;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IAtomicMassUnit;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ICelsius;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDay;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IDegree;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IHectare;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IHour;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ILightYear;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ILiter;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IMinute;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IOpticalPower;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IParsec;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\ITonne;
use PDobrovolny\Quantity\Contracts\Quantity\Metric\Sub\IVoltAmpere;
use PDobrovolny\Quantity\Enums\EMetricExponent;
use PDobrovolny\Quantity\Factory\QuantityFactory;

dataset(
    'relation provider',
    static function () {
        $merge = array_merge(
            metricToMetricRelations(),
            imperialToImperialRelations(),
            imperialToMetricRelations(),
        );

        $result = [];
        foreach ($merge as $index => [$data]) {
            $tmp = [];
            foreach ($data as $key => $value) {
                $tmp[] = [$key, $value];
            }

            foreach (range(1, count($tmp)) as $i) { // rotation
                $result[] = [
                    array_combine(
                        array_map(static fn(array $content) => reset($content), $tmp),
                        array_map(static fn(array $content) => end($content), $tmp),
                    ),
                    $merge[$index][1] ?? null,
                ];
                $tmp[] = array_shift($tmp);
            }
        }

        return $result;
    }
);

it(
    'conversion',
    function (array $relation, ?float $delta = null) {
        $expectedValue = reset($relation);
        $expectedClass = key($relation);
        array_shift($relation);

        $parameters = makeParameters($relation);

        $quantity = $this->factory->create($expectedClass, $parameters);

        $message = var_export($expectedClass, true) . ': ' . implode(', ', array_keys($parameters));

        expect($quantity)
            ->toBeInstanceOf($expectedClass);

        expect($quantity->value)
            ->toEqualWithDelta($expectedValue, $delta ?? (10 ** -12), $message);
    }
)->with('relation provider');

it(
    'create with value',
    function (string $interface) {
        expect($interface)->toBeInterface();

        $quantity = $this->factory->createWithValue($interface, 2.3);

        expect($quantity)
            ->toBeInstanceOf($interface);

        expect($quantity->value)->toEqual(2.3);
    }
)->with('value provider');

it(
    'create with value exponent',
    function (string $interface) {
        expect($interface)->toBeInterface();

        $quantity = $this->factory->createWithValue($interface, 2.3, EMetricExponent::KILO);

        $expectedValue = match (true) {
            $quantity instanceof IMetric && $quantity instanceof IQuantity3D => 2300000000.,
            $quantity instanceof IMetric && $quantity instanceof IQuantity2D => 2300000.,
            $quantity instanceof IMetric => 2300.,
            default => 2.3,
        };

        expect($quantity->value)->toBe($expectedValue);
    }
)->with('value provider');

it(
    'missing',
    function () {
        $this->expectException(InvalidArgumentException::class);
        $this->factory->create(IVolume::class, [new Length(0.)]);
    }
);

it(
    'nan',
    function (array $relation) {
        $expectedClass = array_key_first($relation);
        array_shift($relation);

        $parameters = makeParameters($relation, NAN);

        $message = var_export($expectedClass, true) . ': ' . implode(', ', array_keys($parameters));
        expect($this->factory->create($expectedClass, $parameters))
            ->toBeInstanceOf($expectedClass, $message);
    }
)->with('relation provider');

it(
    'zeros',
    function (array $relation) {
        $expectedClass = array_key_first($relation);
        array_shift($relation);

        $parameters = makeParameters($relation, 0.);

        $message = var_export($expectedClass, true) . ': ' . implode(', ', array_keys($parameters));
        expect($this->factory->create($expectedClass, $parameters))
            ->toBeInstanceOf($expectedClass, $message);
    }
)->with('relation provider');

dataset(
    'value provider',
    function () {
        $result = [];
        foreach (
            [
                __DIR__ . '/../../../src/Contracts/Quantity/Metric',
                __DIR__ . '/../../../src/Contracts/Quantity/Imperial',
            ] as $dir
        ) {
            foreach (getInterfaces($dir) as $iface) {
                $result[] = [$iface];
            }
        }

        return $result;
    }
);

it('makeAssociations', function () {
    expect(array_keys($this->factory->makeAssociations([
        new Time(1),
        new Volume(1),
    ])))->toBe(['time', 'volume']);
});

beforeEach(
    function () {
        $this->factory = new QuantityFactory();
    }
);
function getInterfaces(string $dir): array
{
    $directory = new RecursiveDirectoryIterator($dir);
    $result = [];
    foreach (new RecursiveIteratorIterator($directory) as $info) {
        assert($info instanceof SplFileInfo);

        if ($info->isFile() === false || preg_match('/(I[A-Z]\w+\.php)$/', $info->getRealPath()) !== 1) {
            continue;
        }

        $explodePath = explode('src', $info->getPath());
        $explodeBase = explode('.' . $info->getExtension(), $info->getBasename());

        $namespace = 'PDobrovolny\\Quantity' . str_replace(DIRECTORY_SEPARATOR, '\\', end($explodePath)) . '\\';

        $className = $namespace . reset($explodeBase);
        if (in_array($className, [IMetric::class, IImperial::class, IImperialUS::class, IImperialUK::class,])) {
            continue;
        }

        $result[] = $className;
    }

    return $result;
}

function imperialToImperialRelations(): array
{
    return [
        // basic
        [[IChain::class => 2, IYard::class => 44]],
        [[IChain::class => 20, IFurlong::class => 2]],
//            ICubicChain
//            ICubicFoot
//            ICubicFurlong
//            ICubicInch
//            ICubicLeague
//            ICubicMile
//            ICubicYard
//            IFahrenheit
        [[IFoot::class => 2, IInch::class => 24]],
        [[IFoot::class => 10560, IMile::class => 2]],
        [[IFoot::class => 6, IYard::class => 2]],
        [[IFurlong::class => 16, IMile::class => 2]],
//            [[IGrain::class => 875, IOunce::class => 2]],
        [[IHundredweight::class => 2, IStone::class => 16]],
        [[IHundredweight::class => 40, ITon::class => 2]],
//            IInch
        [[ILeague::class => 2, IMile::class => 6]],
        [[IMile::class => 2, IYard::class => 3520]],
//            [[IOunce::class => 32, IPound::class => 2]],
        [[IPound::class => 28, IStone::class => 2]],
//            ISquareChain
//            ISquareFoot
//            ISquareFurlong
//            ISquareInch
//            ISquareLeague
//            ISquareMile
//            ISquareYard
//            IStone
//            ITon
//            IYard

        // UK
        [[IFluidOunceUK::class => 40, IPintUK::class => 2]],
        [[IGallonUK::class => 2, IQuartUK::class => 8]],
        [[IGillUK::class => 2, IPintUK::class => 8]],
        [[IPintUK::class => 4, IQuartUK::class => 2]],
//            IQuartUK

        // US
        [[IFluidOunceUS::class => 40, IPintUS::class => 2]],
        [[IGallonUS::class => 2, IQuartUS::class => 8]],
        [[IGillUS::class => 2, IPintUS::class => 8]],
        [[IPintUS::class => 4, IQuartUS::class => 2]],
//            IQuartUS
    ];
}

function imperialToMetricRelations(): array
{
    return [
        // basic
        [[IChain::class => 49.709695378987, ILength::class => 1000], 10 ** -6],
//            ICubicChain
//            ICubicFoot
//            ICubicFurlong
//            ICubicInch
//            ICubicLeague
//            ICubicMile
//            ICubicYard
        [[IFahrenheit::class => 86, IThermodynamicTemperature::class => 303.15]],
        [[IFoot::class => 3280.8398950131, ILength::class => 1000], 10 ** -6],
        [[IFurlong::class => 4.9709695378987, ILength::class => 1000], 10 ** -6],
        [[IGrain::class => 15432358.352941, IMass::class => 1000], 10 ** -6],
        [[IHundredweight::class => 19.684130552221, IMass::class => 1000], 10 ** -6],
        [[IInch::class => 39370.078740157, ILength::class => 1000], 10 ** -6],
        [[ILeague::class => 0.20712373074578, ILength::class => 1000], 10 ** -6],
        [[IMile::class => 0.62137119223733, ILength::class => 1000], 10 ** -6],
//            [[IOunce::class => 35273.96194958, IMass::class => 1000], 10 ** -6],
        [[IPound::class => 2204.6226218488, IMass::class => 1000], 10 ** -6],
//            ISquareChain
//            ISquareFoot
//            ISquareFurlong
//            ISquareInch
//            ISquareLeague
//            ISquareMile
//            ISquareYard
        [[IStone::class => 157.47304441777, IMass::class => 1000], 10 ** -6],
        [[ITon::class => 0.98420652761106, IMass::class => 1000], 10 ** -6],
        [[IYard::class => 1093.6132983377, ILength::class => 1000], 10 ** -6],

        // UK
        [[IFluidOunceUK::class => 35195079.727854, IVolume::class => 1000], 10 ** -6],
        [[IGallonUK::class => 219969.24829909, IVolume::class => 1000], 10 ** -6],
        [[IGillUK::class => 7039015.9455708, IVolume::class => 1000], 10 ** -6],
        [[IPintUK::class => 1759753.9863927, IVolume::class => 1000], 10 ** -6],
        [[IQuartUK::class => 879876.99319635, IVolume::class => 1000], 10 ** -6],
//            IQuartUK
//
        // US
        [[IFluidOunceUS::class => 33814022.701843, IVolume::class => 1000], 10 ** -6],
        [[IGallonUS::class => 264172.05235815, IVolume::class => 1000], 10 ** -6],
        [[IGillUS::class => 8453505.6754607, IVolume::class => 1000], 10 ** -6],
        [[IPintUS::class => 2113376.4188652, IVolume::class => 1000], 10 ** -6],
        [[IQuartUS::class => 1056688.2094326, IVolume::class => 1000], 10 ** -6],
//            IQuartUS
    ];
}

function makeParameters(array $relation, ?float $overrideValue = null): array
{
    $parameters = [];
    foreach ($relation as $interface => $value) {
        assert(interface_exists($interface), $interface);
        preg_match('/I(\w+)$/', $interface, $match);
        $parameters[lcfirst($match[1])] = makeQuantity($interface, $overrideValue ?? $value);
    }

    return $parameters;
}

function makeQuantity(string $interface, float $value): IQuantity
{
    return new QuantityFactory()->createWithValue($interface, $value);
}

function metricToMetricRelations(): array
{
    return [
        // basic
//            IAmountOfSubstance
//            IElectricCurrent
        [[ILength::class => 0.05, IAcceleration::class => 2, IPressure::class => 100, IDensity::class => 1000]],
        [[ILength::class => 30856775800, IParsec::class => 10 ** -6], 10 ** -9],
        [[ILength::class => 9.4605E+15, ILightYear::class => 1]],
        [[ILength::class => 299196000000, IAstronomicalUnit::class => 2]],
        [[ILength::class => 2, IForce::class => 4, IWork::class => 8]],
        [[ILength::class => 0.5, IMass::class => 4, IAcceleration::class => 8, IWork::class => 16]],
        [[ILength::class => 0.5, IArea::class => 4, IPressure::class => 8, IWork::class => 16]],
        [[ILength::class => 32, ITime::class => 4, IVelocity::class => 8]],
        [[ILength::class => 0.25, IOpticalPower::class => 4]],
        [
            [
                ILength::class => 0.000125,
                IArea::class => 4,
                IForce::class => 8,
                IDensity::class => 1000,
                IAcceleration::class => 16,
            ],
        ],
        [[ILuminousIntensity::class => 0.5, ILuminousFlux::class => 4, ISolidAngle::class => 8]],
//            IMass
        [[IMass::class => 2000, ITonne::class => 2]],
        [[IMass::class => 1, ILength::class => 2, IAcceleration::class => 4, IWork::class => 8]],
        [[IMass::class => 2, IVelocity::class => 2, IWork::class => 4]],
        [[IMass::class => 1000, IVolume::class => 2, IDensity::class => 500]],
        [[IMass::class => 2, IAcceleration::class => 2, IForce::class => 4]],
        [[IMass::class => 2, IAbsorbedDose::class => 2, IWork::class => 4]],
        [[IMass::class => 2, IEquivalentDose::class => 2, IWork::class => 4]],
        [[IMass::class => 10 ** -16, IAtomicMassUnit::class => 60220285805.476], 10 ** -3],
        [[IThermodynamicTemperature::class => 303.15, ICelsius::class => 30]],
        [[ITime::class => 2, IElectricCurrent::class => 2, ICharge::class => 4]],
        [[ITime::class => 0.5, IRadioactivity::class => 2]],
        [[ITime::class => 172800, IDay::class => 2]],
        [[ITime::class => 2, IAcceleration::class => 2, IVelocity::class => 4]],
        [[ITime::class => 0.5, ILength::class => 2, IVelocity::class => 4]],
        [[ITime::class => 7200, IHour::class => 2]],
        [[ITime::class => 120, IMinute::class => 2]],
        [[ITime::class => 2, IPower::class => 2, IWork::class => 4]],
        [[ITime::class => 0.5, IAmountOfSubstance::class => 2, ICatalyticActivity::class => 4]],

        // derived
//            IAbsorbedDose
//            IAcceleration
        [[IAngle::class => M_PI, IDegree::class => 180]],
        [[IAngle::class => 8, ITime::class => 2, IAngularVelocity::class => 4]],
        [[IAngularVelocity::class => 4 * M_PI, IFrequency::class => 2]],
        [[IArea::class => 0.5, IForce::class => 2, IWork::class => 4]],
        [[IArea::class => 200, IHectare::class => 2]],
        [[IArea::class => 0.5, IForce::class => 2, IPressure::class => 4]],
        [[IArea::class => 0.5, IMagneticFlux::class => 2, IMagneticFluxDensity::class => 4]],
        [[ICapacitance::class => 0.5, ICharge::class => 2, IVoltage::class => 4]],
//            ICatalyticActivity
        [[ICharge::class => 2, IVoltage::class => 2, IWork::class => 4]],
        [[ICharge::class => 2, IVoltage::class => 2, ITime::class => 4]],
        [[IConductance::class => 0.5, IElectricCurrent::class => 2, IVoltage::class => 4]],
        [[IConductance::class => 0.5, IResistance::class => 2]],
//            IDensity
//            IEquivalentDose
        [[IForce::class => 0.5, IPower::class => 2, IVelocity::class => 4]],
//            [[IFrequency::class=>1]],
        [[IIlluminance::class => 2, IArea::class => 2, ILuminousFlux::class => 4]],
//            IInductance
//            ILuminousFlux
        [[IMagneticFlux::class => 8, ITime::class => 2, IVoltage::class => 4]],
//            IMagneticFluxDensity
        [[IPower::class => 8, IElectricCurrent::class => 2, IVoltage::class => 4]],
        [[IPressure::class => 2, IVolume::class => 2, IWork::class => 4]],
//            IRadioactivity
        [[IResistance::class => 1, IElectricCurrent::class => 2, IVoltAmpere::class => 4]],
        [[IResistance::class => 1, IVoltage::class => 2, IVoltAmpere::class => 4]],
        [[IResistance::class => 2, IElectricCurrent::class => 2, IVoltage::class => 4]],
//            ISolidAngle
//            IVelocity
        [[IVoltage::class => 0.5, IElectricCurrent::class => 2, IConductance::class => 4]],
        [[IVoltage::class => 2, IElectricCurrent::class => 2, IVoltAmpere::class => 4]],
        [[IVoltage::class => 2, IElectricCurrent::class => 2, IPower::class => 4]],
        [[IVoltage::class => 8, IElectricCurrent::class => 2, IResistance::class => 4]],
        [[IVolume::class => 2, ILiter::class => 2000]],
        [[IVolume::class => 8, ITime::class => 2, IVolumetricFlowRate::class => 4]],
//            IVolumetricFlowRate
//            IWork

        // sub
        [[IArcMinute::class => 2, IArcSecond::class => 120]],
        [[IArcMinute::class => 120, IDegree::class => 2]],
        [[IArcSecond::class => 7200, IDegree::class => 2]],
//            IAstronomicalUnit
//            IAtomicMassUnit
//            ICelsius
//            IDay
//            IDegree
//            IHectare
//            IHour
//            ILightYear
//            ILiter
//            IMinute
//            IOpticalPower
//            IParsec
//            ITonne
//            IVoltAmpere
    ];
}
