<?php

declare(strict_types=1);

use PDobrovolny\Quantity\Container\Metric\Derived\Angle;
use PDobrovolny\Quantity\Container\Metric\Sub\ArcMinute;
use PDobrovolny\Quantity\Container\Metric\Sub\ArcSecond;
use PDobrovolny\Quantity\Container\Metric\Sub\Degree;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;
use PDobrovolny\Quantity\Factory\ReduceFactory;

it(
    'main',
    function (float $expectedValue, IQuantity $quantity) {
        $reduce = new ReduceFactory()->reduce($quantity);

        expect($reduce)->toBeInstanceOf($quantity::class);
        expect($reduce->value)->toBe($expectedValue);
    }
)->with(
    [
        [0., new Angle(0.)],
        [0., new Angle(2 * M_PI)],
        [M_PI, new Angle(3 * M_PI)],
        [M_PI + M_PI_2, new Angle(-M_PI_2)],

        [0., new Degree(0.)],
        [0., new Degree(360.)],
        [180., new Degree(360. + 180.)],
        [270., new Degree(-90.)],

        [0., new ArcMinute(0.)],
        [0., new ArcMinute(60)],
        [30., new ArcMinute(90)],
        [45., new ArcMinute(-15)],

        [0., new ArcSecond(0.)],
        [0., new ArcSecond(60)],
        [30., new ArcSecond(90)],
        [45., new ArcSecond(-15)],
    ]
);

