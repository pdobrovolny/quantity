<?php

declare(strict_types=1);

use Ds\Set;

expect()->extend('subClassOf', function (string $iface) {
    foreach ($this->value as $value) {
        is_subclass_of($value, $iface)
        || throw new InvalidArgumentException(sprintf('%s is not subclass of %s.', $value, $iface));
    }

    return $this;
});

function getAllQuantities(): array
{
    $directory = new RecursiveDirectoryIterator(__DIR__ . '/../src/Container/');
    $files = new Set();

    foreach (new RecursiveIteratorIterator($directory) as $info) {
        assert($info instanceof SplFileInfo);
        $info->isDir() || $files->add($info);
    }

    $classes = $files->map(fileToClass(...))
        ->sorted();

    return $classes->filter(static fn(string $class) => new ReflectionClass($class)->isFinal())
        ->toArray();
}

dataset(
    'class provider',
    static fn() => array_map(
        static fn(string $class) => [$class],
        getAllQuantities()
    )
);

function fileToClass(SplFileInfo $fileInfo): string
{
    $explode = explode('src', $fileInfo->getPath());

    return 'PDobrovolny\\Quantity' . str_replace(DIRECTORY_SEPARATOR, '\\', end($explode))
        . '\\'
        . $fileInfo->getBasename('.php');
}