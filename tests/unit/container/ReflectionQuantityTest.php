<?php

declare(strict_types=1);

use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantityMulti;

require_once __DIR__ . '/../../TestHelper.php';

it(
    'multi unit',
    function (string $class) {
        expect($class::CLASSES)
            ->toBeArray()
            ->toHaveCount(2)
            ->subClassOf(IQuantity::class);
    }
)->with(
    static fn() => array_map(
        static fn(string $class) => [$class],
        array_filter(
            getAllQuantities(),
            static fn(string $class): bool => is_subclass_of($class, IQuantityMulti::class)
        )
    )
);
