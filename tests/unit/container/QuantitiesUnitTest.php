<?php

declare(strict_types=1);

use PDobrovolny\Quantity\Contracts\Quantity\IQuantity;
use PDobrovolny\Quantity\Contracts\Quantity\IQuantityMulti;
use PDobrovolny\Quantity\Formatter\UnitFormatter;

require_once __DIR__ . '/../../TestHelper.php';

it(
    'unit unique',
    function () {
        $unitFormatter = new UnitFormatter();

        $data = [];
        foreach (getAllQuantities() as $class) {
            $quantity = new $class(0.);
            assert($quantity instanceof IQuantity);

            if ($quantity instanceof IQuantityMulti) {
                continue;
            }

            $data[$unitFormatter->format($quantity)][] = $class;
        }

        expect(
            array_filter(
                $data,
                static fn(array $classes) => count($classes) > 1
            )
        )->toBeEmpty();
    }
);
