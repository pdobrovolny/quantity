# Quantity

[![Php version](https://img.shields.io/packagist/php-v/pdobrovolny/quantity.svg)](https://packagist.org/packages/pdobrovolny/quantity)

[![pipeline status](https://gitlab.com/pdobrovolny/quantity/badges/master/pipeline.svg)](https://gitlab.com/pdobrovolny/quantity/commits/master)
[![coverage report](https://gitlab.com/pdobrovolny/quantity/badges/master/coverage.svg)](https://gitlab.com/pdobrovolny/quantity/commits/master)

[![Donate to this project using Patreon](https://img.shields.io/badge/patreon-donate-yellow.svg)](https://www.patreon.com/pdobrovolny)

Physical quantities and formulas

## Installation

To install, use composer:

```bash
composer require pdobrovolny/quantity
```

## Documentation and usage

A library to represent various quantities as value objects with the ability to convert from one Unit of Measurement to
another.

### International System of Units

#### Base units

- Amount of substance
- Electric current
- Length
- Luminous intensity
- Mass
- Thermodynamic temperature
- Time

#### Derived units

- Absorbed dose
- Acceleration
- Angle
- Area
- Capacitance
- Catalytic activity
- Electric resistance
- Electric charge
- Electric conductance
- Equivalent dose
- Force
- Frequency
- Illuminance
- Inductance
- Luminous flux
- Magnetic flux
- Magnetic flux density
- Power
- Pressure
- Radioactivity
- Solid angle
- Velocity
- Voltage
- Volume
- Volumetric flow rate
- Work

#### Sub-units

- Angle
    - ArcMinute
    - ArcSecond
    - Degree
- Length
    - Astronomical unit
    - Light year
    - Parsec
- Mass
    - Atomic mass unit
    - Tonne
- Time
    - Day
    - Hour
    - Minute
- Celsius
- Hectare
- Liter
- Optical power
- Volt-ampere

### Imperial units

- Area
    - Square Chain
    - Square Foot
    - Square Furlong
    - Square Inch
    - Square League
    - Square Mile
    - Square Yard
- Length
    - Chain
    - Foot
    - Furlong
    - Inch
    - League
    - Mile
    - Yard
- Mass
    - Grain
    - Hundredweight
    - Ounce
    - Pound
    - Stone
    - Ton
- Volume
    - Cubic Chain
    - Cubic Foot
    - Cubic Furlong
    - Cubic Inch
    - Cubic League
    - Cubic Mile
    - Cubic Yard
    - UK
        - FluidOunce
        - Gallon
        - Gill
        - Pint
        - Quart
    - US
        - FluidOunce
        - Gallon
        - Gill
        - Pint
        - Quart
- Fahrenheit

## Examples

### Main examples

#### Basic usage:

```php
$length = new Length(1.);

\var_dump($length->value);     // double(1)
\var_dump($length::UNIT);      // string(1) "m"
```

#### Create a quantity by the factory:
```php
$quantityFactory = new QuantityFactory();

$length = $quantityFactory->createWithValue(ILength::class, 1., EMetricExponent::KILO);

\var_dump($length->value);     // double(1000)
\var_dump($length::class);     // string(50) "PDobrovolny\Quantity\Container\Metric\Basic\Length"
```

#### Create a quantity by a formula:
```php
$quantityFactory = new QuantityFactory();
$length = $quantityFactory->createWithValue(ILength::class, 100000);
$time = $quantityFactory->createWithValue(ITime::class, 3600);

$velocity = $quantityFactory->create(IVelocity::class, \compact('length', 'time'));

\var_dump($velocity->value);        // double(27.777777777778)
\var_dump($velocity::class);        // string(54) "PDobrovolny\Quantity\Container\Metric\Derived\Velocity"
```

#### Customization factory:
```php
final readonly class MyLength implements ILength
{
    public function __construct(public float $value)
    {
    }
}

$quantityFactory = new QuantityFactory([
    ILength::class => autowire(MyLength::class),
]);

$length = $quantityFactory->createWithValue(ILength::class, 1.);
\var_dump($length::class);          // string(16) "example\MyLength"
```

### Converting examples

#### Mile to Length:
```php
$quantityFactory = new QuantityFactory();
$mile = $quantityFactory->createWithValue(IMile::class, 1);

$length = $quantityFactory->create(ILength::class, \compact('mile'));

\var_dump($length->value);      // double(1609.344)
```

#### Celsius to Fahrenheit:
```php
$quantityFactory = new QuantityFactory();

$celsius = $quantityFactory->createWithValue(ICelsius::class, 20);
$thermodynamicTemperature = $quantityFactory->create(IThermodynamicTemperature::class, compact('celsius'));
$fahrenheit = $quantityFactory->create(IFahrenheit::class, compact('thermodynamicTemperature'));

\var_dump($fahrenheit->value);      // double(68)
```

### Formatting example

#### Creating of QuantityFormatter:
```php
$formatter = new QuantityFormatter(
    new \NumberFormatter('cs_CZ', \NumberFormatter::PATTERN_DECIMAL)
);

```
#### Scaling a quantity:
```php
$quantity = new Length(1.);

echo $formatter->format($quantity) . "\n\n";

echo $formatter->format($quantity, EMetricExponent::MILI) . "\n";
echo $formatter->format($quantity, EMetricExponent::BASE) . "\n";
echo $formatter->format($quantity, EMetricExponent::DEKA) . "\n";
echo $formatter->format($quantity, EMetricExponent::KILO) . "\n";
```

Output:
```txt
1 m

1 000 mm
1 m
0,1 dam
0,001 km
```

## Support

I prefer to keep my work available to everyone. In order to do so I rely on voluntary contributions
on [Patreon](https://www.patreon.com/pdobrovolny).
