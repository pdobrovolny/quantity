<?php

declare( strict_types=1 );

use Rector\Config\RectorConfig;
use Rector\Php74\Rector\LNumber\AddLiteralSeparatorToNumberRector;
use Rector\Php81\Rector\Array_\FirstClassCallableRector;
use Rector\Php81\Rector\FuncCall\NullToStrictStringFuncCallArgRector;
use Rector\PHPUnit\Set\PHPUnitSetList;
use Rector\Set\ValueObject\LevelSetList;

return static function ( RectorConfig $rectorConfig ): void {
	$rectorConfig->paths( [
		getcwd() . '/src',
		getcwd() . '/tests',
	] );

	$rectorConfig->sets( [
		LevelSetList::UP_TO_PHP_84,
		PHPUnitSetList::PHPUNIT_110,
	] );
	$rectorConfig->skip( [
		AddLiteralSeparatorToNumberRector::class,
		FirstClassCallableRector::class,
		NullToStrictStringFuncCallArgRector::class,
	] );
};
